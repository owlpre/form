<?php

namespace Hub3C\Console\Commands;

use Illuminate\Console\Command;

class AlpacaJS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'alpacajs:build';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Building Alpaca JS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Building AlpacaJS ...');
        $this->line(getcwd());
        chdir('public/alpacajs');
        $this->line('directory changed to -> '.getcwd());
        $this->warn('start building ...');
        $this->_exec('npm start');
        chdir('../..');
        $this->line('directory changed to -> '.getcwd());
        $this->warn('copying build assets ...');
        $this->_exec('rsync -av public/alpacajs/build/ public/alpacajs-build/');
        $this->info('finish building ...');
    }

    private function _exec($cmd) {
        while (@ ob_end_flush());
        $proc = popen($cmd, 'r');
        while (!feof($proc)) {
            echo fread($proc, 4096);
            @ flush();
        }
    }
}

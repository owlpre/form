<?php

// Contract for Hub's repositories.
// Limit this interface by handle ONLY the base functionalities.

namespace Hub3C\Core\Base\Repository\Contract;

interface RepositoryInterface
{
	public function create (array $attributes);

	public function all ();

	public function find ($id);

	public function findBy ($column, $value);

	public function destroy ($id);
}

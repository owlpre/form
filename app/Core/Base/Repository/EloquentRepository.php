<?php

// This is the BASE repository class. Every model-specific
// functionalities should be reside inside the Model's repository class only.

namespace Hub3C\Core\Base\Repository;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;
use Hub3C\Core\Base\Repository\Contract\RepositoryInterface as Repository;

abstract class EloquentRepository implements Repository
{
	protected $app;
	protected $model;

	public function __construct (App $app)
	{
		$this->app = $app;
		$this->makeModel();
	}

	// Declare your model here
	abstract function model ();

	protected final function makeModel ()
	{
		// Call our container
		$model = $this->app->make($this->model());

		if (!$model instanceof Model)
			throw new \Exception("Class $model must be an instance of Eloquent Model.");

		$this->model = $model;
		return true;
	}

	// Start Eloquent base methods

	public function create (array $attributes)
	{
		return $this->model->create($attributes);
	}

	public function all ()
	{
		return $this->model->all();
	}

	public function find ($id)
	{
		return $this->model->find($id);
	}

	public function findBy ($column, $value)
	{
		return $this->model->where($column, $value);
	}

	public function destroy ($id)
	{
		return $this->find($id)->delete();
	}
}

<?php

namespace Hub3C\Core\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use Hub3C\Link, Hub3C\Form;
use Hub3C\Observers\LinkObserver, Hub3C\Observers\FormObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::addExtension('blade.js', 'blade');
        Form::observe(FormObserver::class);
        Link::observe(LinkObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

<?php

namespace Hub3C\Core\Providers;

use View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $views = [
            'homepage',
        ];

        View::composer($views, 'Hub3C\Domain\Homepage\Composers\HomepageExpertees');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

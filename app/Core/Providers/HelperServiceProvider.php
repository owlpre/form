<?php

namespace Hub3C\Core\Providers;

use Illuminate\Support\ServiceProvider;

class HelperServiceProvider extends ServiceProvider
{
	public function boot ()
	{
		//
	}

	public function register ()
	{
		// Register your helper classes here
		$this->registerHelperClass('Hub3C\Infrastructure\Helpers\Classes\MyFooHelper');
	}

	// The actual register method
	protected function registerHelperClass ($className)
	{
		$rawClassName = substr($className, strrpos($className, '\\') + 1);

		$this->app->singleton(snake_case($rawClassName), 
					function ($app) use ($className) {
					return new $className();
		});
	}
}

<?php

namespace Hub3C\Core\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $webAppNamespace = 'Hub3C\Http\Controllers\Web';
    protected $webSrvNamespace = 'Hub3C\Http\Controllers\Api';

    public function boot()
    {
        parent::boot();
    }

    public function map()
    {
        $this->mapApiRoutes();
        $this->mapWebRoutes();
    }

    protected function mapApiRoutes ()
    {
        Route::group([
            'prefix' => 'api',
            'middleware' => ['api', 'cors'],
            'namespace' => $this->webSrvNamespace,
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }

    protected function mapWebRoutes ()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->webAppNamespace,
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }
}

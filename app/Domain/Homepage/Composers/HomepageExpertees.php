<?php

namespace Hub3C\Domain\Homepage\Composers;

use Illuminate\View\View;
use Hub3C\Domain\Homepage\Repositories\HomepageRepository;

class HomepageExpertees
{
	protected $repo;

	public function __construct (HomepageRepository $repo)
	{
		$this->repo = $repo;
	}

	public function compose (View $view)
	{
		$view->with('expertees', $this->repo->getSomeFoo());
	}
}

<?php

namespace Hub3C\Domain\Submission\Models;

use Hub3C\Form;
use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    protected $fillable = [
        'form_id', 'data',
    ];

    public function form ()
    {
    	return $this->belongsTo(Form::class);
    }
}

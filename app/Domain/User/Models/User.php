<?php

namespace Hub3C\Domain\User\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 
        'name', 
        'email', 
        'password', 
        'business_id',
        'created_on_hub',
        // 'app_permissions',
        'timezone_setting',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function forms() {
        return $this->hasMany('Hub3C\Form');
    }

    public function form_submissions() {
        return $this->hasManyThrough(
            \Hub3C\Domain\Submission\Models\Submission::class,
            \Hub3C\Form::class
        );
    }
}

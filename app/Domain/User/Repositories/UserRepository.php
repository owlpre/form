<?php

namespace Hub3C\Domain\User\Repositories;

use Hub3C\Core\Base\Repository\EloquentRepository as Repository;

class UserRepository extends Repository
{
	function model ()
	{
		return 'Hub3C\Domain\User\Models\User';
	}
}

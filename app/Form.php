<?php

namespace Hub3C;

use stdClass;
use Illuminate\Database\Eloquent\Model;
use Hub3C\Domain\Submission\Models\Submission;
use Storage;

class Form extends Model
{

    protected $fillable = [
        'user_id', 'title', 'data', 'draft', 'extras',
    ];
    protected $appends = [
        'parsed_data', 'field_count',
    ];

    public function guid() {
        $link = Link::where([
            ['form_id', '=', $this->id],
            ['user_id', '=', $this->user_id],
        ])->get()->first();
        return $link->id;
    }

    public function getFieldCountAttribute() {
        return count((array) $this->parsed_data->schema->properties);
    }

    public function getParsedDataAttribute() {
        return json_decode(stripslashes($this->data()));
    }

    public function extras() {
        return addslashes(
            $this->extras!=='null'
            ?$this->extras
            :"{}"
        );
    }

    public function data() {
        $default = json_decode(Storage::get('/etc/default-formbuilder.json'));
        return addslashes(
            $this->data!=='null'
            ?$this->data
            :json_encode($default)
        );
    }

    public function submissions () {
    	return $this->hasMany(Submission::class);
    }
}

<?php

namespace Hub3C\Http\Controllers\Api;

use Illuminate\Http\Request;
use Hub3C\Http\Controllers\Controller;
use Hub3C\Infrastructure\Services\SubmissionService;

class SubmissionController extends Controller
{
	protected $submissionService;

	public function __construct (SubmissionService $submissionService)
	{
		$this->submissionService = $submissionService;
	}

	// Webhook, API POST mode
	public function getSubmission ($id, Request $request)
	{
		$this->submissionService->storeSubmission($id, $request);

		return redirect()->route('submission.status');
	}
}

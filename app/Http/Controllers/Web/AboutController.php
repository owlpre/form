<?php

namespace Hub3C\Http\Controllers\Web;

use Hub3C\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Storage;

class AboutController extends Controller
{
    public function buildNumber() {
        $number = false;
        if (Storage::exists('version.md')) {
            $number = str_replace(
                "\n", '', Storage::get('version.md')
            );
        }
        return view("about.build-number", [
            'number' => $number
        ]);
    }
}

<?php

namespace Hub3C\Http\Controllers\Web;

use Illuminate\Http\Request;
use Hub3C\Http\Controllers\Controller;
use Hub3C\Infrastructure\Services\Hub\HubClient;

class DumpTokenPageController extends Controller
{
	protected $hubClient;

	public function __construct (HubClient $hubClient)
	{
		$this->hubClient = $hubClient;
	}

	public function getPage (Request $request)
	{
		$hubUser = $this->hubClient->getCurrentUser();
		return view('dumptoken', ['currentUser' => $hubUser]);
	}

}

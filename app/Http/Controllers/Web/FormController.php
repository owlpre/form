<?php

namespace Hub3C\Http\Controllers\Web;

use Hub3C\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Hub3C\Form;
use Hub3C\Link;
use stdClass;
use Mail;
use Hub3C\Mail\ShareForm;
use Validator, Storage;

class FormController extends Controller
{
    public function activate(Form $form) {
        $form->active = !$form->active;
        $form->save();
        return redirect('/');
    }

    public function extras() {
        $user = auth()->user();
        $form_id = request()->input('id');
        $extras = request()->input('extras');
        $form = $user->forms()->findOrFail($form_id);
        $form->update([
            'extras' => $extras,
        ]);
        if (!$user->id) {
            return abort('403');
        }
        return response()->json(request()->input());
    }

    public function image($user_id, $key) {
        $user = auth()->user();
        if (!$user->id && $user_id !== $user->id) {
            return abort('403');
        }
        return Storage::get('/images/users/'.$user_id.'/'.$key);
    }

    public function images() {
        $user = auth()->user();
        if (!$user->id) {
            return response()->json([
                'result' => false,
                'message' => 'Unknown User!',
            ]);
        }
        $images = Storage::files('/images/users/'.$user->id);
        foreach ($images as $i => $image) {
            $images[$i] = view('template.choose-image-item', [
                'image' => $image,
            ])->render();
        }
        return response()->json([
            'result' => true,
            'images' => $images,
        ]);
    }

    public function uploadImage() {
        $user = auth()->user();
        $validator = Validator::make(request()->all(), [
            'file' => 'required|image|mimes:jpeg,bmp,png',
        ]);
        $response = [
            'result' => true,
        ];
        if ($validator->fails()) {
            $response = [
                'result' => false,
                'errors' => $validator->errors(),
            ];
        } else {
            request()->file->storeAs(
                '/images/users/'.$user->id
                , request()->file->getClientOriginalName()
            );
        }
        return response()->json($response);
    }

    public function loadMore() {
        $form = auth()->user()->forms()->findOrFail(request()->input('id'));
        $forms = auth()->user()->forms()
            ->where('created_at', '<', $form->created_at)
            ->where('id', '!=', $form->id)
            ->orderBy('created_at', 'desc')->limit(10)->get();
        foreach ($forms as $i => $form) {
            $form->view = view('form.item', ['form' => $form])->render();
        }
        $formsCount = auth()->user()->forms()->count();
        $fetched = request()->input('fetched') + count($forms);
        return response()->json([
            'forms' => $forms,
            'allLoaded' => ($formsCount <= $fetched),
        ]);
    }

    public function index() {
        $forms = auth()->user()->forms()
            ->orderBy('updated_at', 'desc')->get();
        return view('form.index', [
            'forms' => $forms,
        ]);
    }

    public function sendTo(Form $form) {
        return view('form.send', [
            'form' => $form,
        ]);
    }

    public function send(Form $form) {
        $emails = json_decode(request()->input('recipient'));
        if (empty($emails)) {
            request()->replace(['recipient'=> '']);
        }

        $this->validate(request(), [
            'recipient' => 'required',
            'message' => 'required',
        ]);

        Mail::to($emails)
            ->send(new ShareForm(
                request()->input('message')
                , $form
            ));


        request()->session()->flash('alert', [
            'title' => 'Email Sent',
            'message' => 'The email for sharing the form was successfully sent!',
        ]);
        return redirect('/');
    }

    public function link(Form $guid) {
        $link = Link::findOrFail($guid);
    }

    public function show($guid) {
        $link = Link::findOrFail($guid);
        $form = $link->form;
        if ($form->draft) {
            $form->update([
                'draft' => false,
            ]);
        }
        return view('form.show', [
            'form' => $form,
        ]);
    }

    public function edit(Form $form) {
        $form = auth()->user()->forms()->find($form->id);
        if (!$form) {
            return abort(403);
        }
        return view('form.edit', [
            'form' => $form,
            'inside' => true,
        ]);
    }

    public function validateFormCreate() {
        $user = auth()->user();
        $validator = Validator::make(request()->all(), [
            'name' => 'required|max:100|unique:forms,title,NULL,id,user_id,'
                .$user->id,
        ]);
        $response = [
            'result' => true,
        ];
        if ($validator->fails()) {
            $response = [
                'result' => false,
                'errors' => $validator->errors(),
            ];
        }
        return response()->json($response);
    }

    public function store() {
        $user = auth()->user();
        $form = $user->forms()->create([
            'title' => request()->input('name'),
        ]);
        return redirect('/form/'.$form->id.'/edit');
    }

    public function preview(Request $request) {
        $id = $request->input('id');
        $form = Form::findOrFail($id);
        return view('form.show', [
            'form' => $form,
            'preview' => true,
        ]);
    }

    public function autoSave(Form $form, Request $request) {
        $data = $request->input('data');
        $form->update([
            'data' => $data,
        ]);
        return response()->json($data);
    }

    public function goHome() {
        request()->session()->flash('alert', [
            'title' => 'Form Saved',
            'message' => 'The form has been successfully saved!',
        ]);
        return response()->json(url('/'));
    }
}

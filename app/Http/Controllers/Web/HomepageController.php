<?php

namespace Hub3C\Http\Controllers\Web;

use Hub3C\Http\Controllers\Controller;
use Hub3C\Form;
use Hub3C\Infrastructure\Services\SubmissionService;

class HomepageController extends Controller
{
    protected $submissions;

    public function __construct (SubmissionService $submissions)
    {
        $this->submissions = $submissions;
    }

	public function getHomepage ()
	{
        $user = auth()->user();
        $forms = $user->forms()->orderBy('created_at', 'desc')->limit(5)->get();
        foreach ($forms as $i => $form) {
            $form->view = view('form.item', ['form' => $form])->render();
        }
        $formsCount = auth()->user()->forms()->count();
        $submissions = $this->submissions->getAllSubmission(5);
		return view('homepage', [
            'forms' => $forms,
            'allLoaded' => $formsCount <= count($forms),
            'submissions'=> $submissions,
        ]);
	}

}

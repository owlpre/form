<?php

namespace Hub3C\Http\Controllers\Web;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Hub3C\Domain\User\Models\User;
use Illuminate\Support\Facades\Hash;
use Hub3C\Http\Controllers\Controller;
use Hub3C\Infrastructure\Services\Hub\HubTokenValidatorService;
use Hub3C\Infrastructure\Services\Hub\HubUserService;

class HubAccessGatewayController extends Controller
{
	protected $hubUser;
	protected $validator;

	public function __construct (HubTokenValidatorService $validator,
								 HubUserService $hubUser)
	{
		$this->hubUser = $hubUser;
		$this->validator = $validator;
	}

	protected function syncUserAndGiveAccess ($user)
	{
		$localUser = User::firstOrNew([
			'id' => $user->systemUserId,
		]);

		$localUser->name = $user->firstName . ' ' . $user->lastName;
		$localUser->email = $user->emailAddress;
		$localUser->business_id = $user->businessId;
		$localUser->created_on_hub = Carbon::parse($user->createdOn)->toDateTimeString();
		$localUser->timezone_setting = $user->defaultTimezone;
		$localUser->password = Hash::make('ZXasqw12');
		$localUser->save();

		return $this->letUserIn($localUser->id);
	}

	protected function letUserIn ($id)
	{
		if (Auth::loginUsingId($id))
		{
			\Log::info(__class__ . ': User ' . $id . ' has logged in.');
			return redirect()->intended('/');
		}
		\Log::warning(__class__ . ': User ' . $id . ' has failed to log in.');
		return "Can't fetch your profile, sorry.";
	}

	public function getHubTokenAndSyncUser (Request $request)
	{
		$access = $this->validator->checkHubToken($request);
		if (!$access) {
			return "Error: Token is not verified.";
		}
		
		$user = $this->hubUser->getUserInfo($access->sub, $request->token);
		if (!$user)
		{
			\Log::warning(__class__ . ': I have user ID. Will try to fallback to local user data.');
			return $this->letUserIn($access->sub);
		} else {
			\Log::info(__class__ . ': Profile data returned, now sync and try to log in.');
			return $this->syncUserAndGiveAccess($user);
		}
	}
}

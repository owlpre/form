<?php

namespace Hub3C\Http\Controllers\Web;

use Hub3C\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PrototypeController extends Controller
{
    public function toggle() {
        return view('prototype.toggle');
    }

    public function signature() {
        return view('prototype.signature');
    }

    public function alpacaFormBuilderIndex() {
        return view('prototype.alpaca.formbuilder.index');
    }

    public function alpacaFormBuilderDesigner() {
        return view('prototype.alpaca.formbuilder.designer');
    }

    public function getHubformPrototype () {
    	return view('prototype.hubform.designer');
    }
}

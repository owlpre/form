<?php

namespace Hub3C\Http\Controllers\Web;

use Hub3C\Form;
use Illuminate\Http\Request;
use Hub3C\Http\Controllers\Controller;
use Hub3C\Infrastructure\Services\SubmissionService;
use Hub3C\Infrastructure\Services\AlpacaTransformerService;
use Html, Storage;

class SubmissionController extends Controller
{
	protected $alpaca;
	protected $submissionService;

	public function __construct (SubmissionService $submissionService,
								 AlpacaTransformerService $alpaca)
	{
		$this->submissionService = $submissionService;
		$this->alpaca = $alpaca;
	}

	public function getSubmissionList ()
	{
		$submissions = $this->submissionService->getAllSubmission();
		return view('submission.index', 
					['submissions' => json_encode($submissions)]
					);
	}

	protected function getFormInformation ($id)
	{
		return Form::where('id', $id)->first();
	}

	public function getSubmissionListByForm ($id)
	{
		$submissions = $this->submissionService->getAllSubmissionByFormId($id);
		$formTitle = $this->getFormInformation($id)->title;

		return view('submission.index', [
				'formTitle' => $formTitle,
				'submissions' => json_encode($submissions), 
				'byForm' => true,
				'fields' => json_decode($this->alpaca->getAlpacaFormFields($id)),
				'excludes' => $this->alpaca->getExcludeFieldsForExport($id, ['signature'])
		]);
	}

	public function getSubmissionDetail ($offset)
	{
		$submissions = $this->submissionService->getAllSubmission();
		$fields = json_decode($this->alpaca->getAlpacaFormFields(1));

		return view('submission.detail', [
			'offset' => $offset,
			'submission' => json_encode($submissions),
			'fields' => $fields,
		]);
	}

	public function getFile($key) {
		$key = 'submissions/'.str_replace('submissions/', '', $key);
		if (!Storage::exists($key)) {
			return abort('404');
		}
		return Storage::get($key);
	}

	public function getSubmissionDetailByForm($id, $offset)
	{
		$submissions = $this->submissionService->getAllSubmissionByFormId($id);
		$formTitle = $this->getFormInformation($id)->title;
		$formProperties = $this->alpaca->getAlpacaFormProperties($id);
		$signatures = [];
		$files = [];
		foreach ($formProperties as $i => $field) {
			if ($field->type === 'signature') {
				$signatures[] = $i;
			}
			if ($field->type === 'file') {
				$files[] = $i;
			}
		}
		foreach ($submissions as $i => $submission) {
			foreach ($signatures as $_i => $signature) {
				if ($submissions[$i][$signature]) {
					$submissions[$i][$signature] = 
						"<img"
						." style='width: 100%;'"
						." src='".$submissions[$i][$signature]."'>";
				}
			}
			foreach ($files as $_i => $file) {
				if ($submissions[$i][$file]) {
					$filename = str_replace(
						'submissions/', '', $submissions[$i][$file]
					);
					$submissions[$i][$file] = 
						Html::link(
							'/submissions/get/file/'.$filename
							, 'Download File'
							, [
								'class' => 'btn btn-secondary btn-sm',
								'download' => $filename,
							]
						)->toHtml();
				}
			}
		}
		return view('submission.detail', [
			'formTitle' => $formTitle,
			'offset' => $offset,
			'submission' => json_encode($submissions),
			'fields' => json_decode($this->alpaca->getAlpacaFormFields($id))
		]);
	}	
}

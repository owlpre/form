<?php

namespace Hub3C\Http\Controllers\Web;

use Illuminate\Http\Request;
use Hub3C\Http\Controllers\Controller;

class ToolsController extends Controller
{
    public function titleChanger() {
        return view('tools.title-changer');
    }
}

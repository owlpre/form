<?php

namespace Hub3C\Http\Middleware;

use Cookie;
use Closure;
use Hub3C\Infrastructure\Services\Hub\HubAccessService;

class RedirectIfHubAccessIsValid // Change this name to CheckHubToken
{
	protected $hubAccess;

	public function __construct (HubAccessService $hubAccess)
	{
		$this->hubAccess = $hubAccess;
	}

	public function handle ($request, Closure $next)
	{
		if (!Cookie::get(config('hub3c.hubTokenCookieName'))) {
			$this->hubAccess->checkToken($request);
		}

		return $next($request);
	}
}

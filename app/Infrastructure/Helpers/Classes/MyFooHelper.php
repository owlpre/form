<?php

// Instead of using class like this, you can just create a function
// in helpers.php (recommended).

namespace Hub3C\Infrastructure\Helpers\Classes;

class MyFooHelper
{
	// A simple and stupid helper to display Laravel's current version
	
	public function laravelVersion ()
	{
		return app()::VERSION;
	}
}

<?php

if (!function_exists('guid')) {
    function guid()
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }
        return sprintf(
            '%04X%04X-%04X-%04X-%04X-%04X%04X%04X'
            , mt_rand(0, 65535)
            , mt_rand(0, 65535)
            , mt_rand(0, 65535)
            , mt_rand(16384, 20479)
            , mt_rand(32768, 49151)
            , mt_rand(0, 65535)
            , mt_rand(0, 65535)
            , mt_rand(0, 65535)
        );
    }
}

if (!function_exists('alert')) {
    function alert() {
        $data = request()->session()->get('alert');
        return View::make('alert.base', [
            'type' => @$data['type']?:'success',
            'title' => @$data['title']?:'Alert',
            'message' => @$data['message']?:'hello!',
        ])->render();
    }
}

if (!function_exists('active')) {
    function active ($key) {
        $lists = [
            'dashboard' => [
                'users.home',
            ],
            'form' => [
                'form.index',
            ],
            'submission' => [
                'submission.list',
                'submission.detail',
            ],
        ];
        if (in_array(Route::currentRouteName(), $lists[$key])) {
            return ' active';
        }
        return '';
    }
}

// Our custom helper function goes here

function app_version ()
{
	return "0.1 Pluto";
}

function hello_world (string $name=null)
{
	return (isset($name)) ?
		  	"Hello, " . $name . "!" :
		  	"Hello, World!";
}

//

if (!function_exists('laravel_version')) 
{
	function laravel_version() {
		return app('my_foo_helper')->laravelVersion();
	}
}

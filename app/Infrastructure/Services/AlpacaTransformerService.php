<?php

namespace Hub3C\Infrastructure\Services;

use Hub3C\Form;
use Hub3C\Infrastructure\Services\MsTzConvertService;

class AlpacaTransformerService
{
	protected $msTz;

	public function __construct (MsTzConvertService $msTz)
	{
		$this->msTz = $msTz;
	}

	public function getAlpacaFormFields ($formId)
	{
		$fieldProps = $this->getAlpacaFormProperties($formId);
		$formFields = new \stdClass;

		foreach ($fieldProps as $k => $v)
		{
			// if ($v->type !== 'personalname' && count((array)$fieldProps) != 1) {
				$formFields->$k = isset($v->label) 
								  ? $v->label 
								  : "Field".$v->order;
			// }
		}

		return json_encode($formFields);
	}

	public function getExcludeFieldsForExport ($formId, $fieldTypes)
	{
		$fieldProps = $this->getAlpacaFormProperties($formId);
		$fields = ['state', 'submitted_by'];
		$excludes = [];

		foreach ($fieldProps as $k => $v)
		{
			if (in_array($v->type, $fieldTypes)) $excludes[] = $k;
		}

		return array_merge($fields, $excludes);
	}	

	public function getAlpacaFormProperties ($formId)
	{
		$form = Form::find($formId);
		$props = json_decode($form->data)
				 ->options
				 ->fields;

		return collect($props)->sortBy('order');
	}

	public function getAlpacaFieldProperties ($formId, $fieldId)
	{
		$field = new \stdClass;
		$form = json_decode($this->getAlpacaFormProperties($formId));

		if (isset($form->$fieldId)) {
			$field->type = $form->$fieldId->type;
			$field->label = isset($form->$fieldId->label) 
							? $form->$fieldId->label 
							: "Field".$form->$fieldId->order;
			return $field;
		} else {
			return false;
		}
	}

	public function isPersonNameField ($id, $index)
	{
		$fieldType = $this->getAlpacaFieldProperties($id, $index);

		if($fieldType) {
			return $fieldType->type === "submittedby";
		}
	}

	public function transformDataField ($dataJson, $formId, $fieldId)
	{
		$dataCollection = collect(json_decode($dataJson, true));
		$data = [];

		$dataCollection->each(function ($value, $index)
			use (&$data, $formId, $fieldId) {
			$p = false;
			$hasPersonalName = $this->isPersonNameField($formId, $index);
			
			if ($hasPersonalName) 
			{
				if ($p === false) {
					if (!$value) {
						$data[$index] = "Submission " . $fieldId;
					} else {
						$data[$index] = $value;
					}
					$data['submitted_by'] = $value;
					$p = true;
				}
			} else {
				$data[$index] = $value;
			}
		});
		$data['submitted_by'] = @$data['submitted_by']?:"Submission ".$fieldId;
		return $data;
	}

	public function transformAlpacaField ($collection)
	{
		$data = [];

		$collection->each(function($field) use(&$data) {
			$arrayData = $this->transformDataField($field->data, 
												   $field->form_id,
												   $field->form_item_id);

			$data[] = array_merge([
					'id' => $field->id,
					'form_id' => $field->form_id,
					'form_title' => Form::find($field->form_id)->title,
					'item_id' => $field->form_item_id,
					'date' => $this->msTz->convertDateByMsTz($field->created_at)->toDayDateTimeString(),
					// 'fields' => $this->getAlpacaFormFields($field->form_id),
				],
				$arrayData
				);
		});
		return $data;
	}
}

<?php

namespace Hub3C\Infrastructure\Services\Hub;

use Cookie;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Hub3C\Infrastructure\Services\JWT\HubJWTService;

class HubAccessService
{
	protected $hubJwtSrv;
	protected $cookieName;
	protected $dt;

	public function __construct (HubJWTService $hubJwtSrv)
	{
		$this->hubJwtSrv = $hubJwtSrv;
		$this->cookieName = config('hub3c.hubTokenCookieName');
		$this->dt = new Carbon;
	}

	protected function checkAndStoreToken ($tokenData, $store = false)
	{
		try {
			$decoded = $this->hubJwtSrv->decode($tokenData);
			$exp = $this->dt->createFromTimestamp($decoded->decoded->exp);

			if ($decoded && $store)
			{
				Cookie::queue(
					Cookie::make(
						$this->cookieName,
						$tokenData,
						60
					)
				);

				return true;
			}
		}
		catch (\ErrorException $e)
		{
			\Log::warning(__class__ . ': ERROR ' . $e->getMessage());
			return false;
		}
	}

	public function checkToken ($requestObject)
	{
		if (null === $requestObject->token /*and 
			null === Cookie::get($this->cookieName)*/)
		{
			\Log::warning(__class__ . ': No token provided.');
			return false;
		}
		else {
			$this->getHubToken($requestObject);
		}
	}

	protected function getHubToken ($requestObject)
	{
		if (Cookie::get($this->cookieName) !== null)
		{
			return Cookie::get($this->cookieName);
			// return true;
		}

		if ($requestObject->token !== null)
		{
			$this->checkAndStoreToken($requestObject->token, true);
			// return false;
		}
	}
}

<?php

namespace Hub3C\Infrastructure\Services\Hub;

use Cookie;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Hub3C\Infrastructure\Services\JWT\HubJWTService;

// This is just a test class

class HubClient
{
	protected $jwt;
	protected $client;
	protected $tokenInfo;
	protected $cookieName;
	protected $hubTPEndpoint;

	public function __construct (HubJWTService $jwt)
	{
		$this->jwt = $jwt;
		$this->tokenInfo = new \stdClass;

		$this->cookieName = config('hub3c.hubTokenCookieName');
		$this->hubTPEndpoint = config('hub3c.hub3rdPartyEndpoint');
		
		$this->client = new Client(['base_uri' => $this->hubTPEndpoint]);
	}

	protected function getResponse ($response)
	{
		return json_decode($response->getBody());
	}

	protected function decode ($token)
	{
		$data = $this->jwt->decode($token);
		
		if (!$data) return false;
		return $data;
	}

	protected function getToken ()
	{
		$cookie = Cookie::get($this->cookieName);
		$data = $this->decode($cookie);		

		if (@$data) {
			$this->tokenInfo->token = $cookie;
			$this->tokenInfo->decoded = $data->decoded;
			$this->tokenInfo->subject = $data->decoded->sub;
		}
	}

	public function getCurrentUser ()
	{
		$user = new \stdClass;
		$this->getToken();

		if (!@$this->tokenInfo->decoded) return false;

		$request = $this->client->get(
					'Account/UserDetail/' . $this->tokenInfo->subject, 
					['headers' => 
						[ 'Authorization' => 'Bearer ' . $this->tokenInfo->token]
					]
				);

		$response = json_decode($request->getBody());

		// $user->id = $response->meta->user->userId;
		$user->id = $this->tokenInfo->subject;
		$user->profile = $response->data[0];

		return $user;
	}
}

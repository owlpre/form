<?php

namespace Hub3C\Infrastructure\Services\Hub;

use Illuminate\Http\Request;
use Hub3C\Infrastructure\Services\JWT\HubJWTService;

class HubTokenService
{
	protected $jwt;

	public function __construct (HubJWTService $jwt)
	{
		$this->jwt = $jwt;
	}

	public function getRaw ($requestObject)
	{
		$cookie = $requestObject->cookie(config('hub3c.hubTokenCookieName'));
		
		if ($cookie)
			return $cookie;
	}

	public function getDecoded ($requestObject)
	{
		$cookie = $this->getRaw($requestObject);

		if ($cookie)
		{
			return $this->jwt->decode($cookie);
		}
	}
}

<?php

namespace Hub3C\Infrastructure\Services\Hub;

use Hub3C\Infrastructure\Services\JWT\HubJWTService;

class HubTokenValidatorService
{
	protected $hubJWT;

	public function __construct (HubJWTService $hubJWT)
	{
		$this->hubJWT = $hubJWT;
	}

	public function checkHubToken ($requestObject)
	{
		if (null === $requestObject->token)
		{
			return false;
		} 
		else {
			return $this->validateAndDecodeToken($requestObject->token);
		}
	}

	protected function validateAndDecodeToken ($token)
	{
		try {
			$token = $this->hubJWT->decode($token);

			if ($token) {
				return $token->decoded;
			}
		}
		catch (\ErrorException $e)
		{
			\Log::warning(__class__ . ' : ERROR ' . $e->getMessage());
			return false;
		}
	}
}

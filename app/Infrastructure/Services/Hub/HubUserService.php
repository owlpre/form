<?php

namespace Hub3C\Infrastructure\Services\Hub;

use Illuminate\Http\Request;
use GuzzleHttp\Client as APIclient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;

class HubUserService
{
	protected $hubUser;
	protected $apiClient;
	protected $hubTPEndpoint;

	public function __construct ()
	{
		$this->endpoint = config('hub3c.hub3rdPartyEndpoint');
		$this->apiClient = new APIclient(['base_uri' => $this->endpoint]);
	}

	public function getUserInfo ($sid, $bearerToken)
	{
		try {
			$request = $this->apiClient->get(
				'Account/UserDetail/' . $sid,
				['headers' =>
					['Authorization' => 'Bearer ' . $bearerToken]
				]
			);

			$response = json_decode($request->getBody());
			return $response->data[0];
		}
		catch (ClientException $e)
		{
			\Log::warning(__class__ . ' getUserInfo: Failed result.');
			return false;
		}
		catch (ConnectException $e)
		{
			\Log::error(__class__ . ' getUserInfo: API server went AWOL.');
			return false;
		}
	}

}

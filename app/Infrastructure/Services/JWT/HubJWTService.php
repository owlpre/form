<?php

namespace Hub3C\Infrastructure\Services\JWT;

// Hub uses JWK. Be careful when you're installing
// JWT package for PHP. Be sure to use
// fproject/php-jwt, not firebase/php-jwt.

use Firebase\JWT\JWT;
use Firebase\JWT\JWK;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

class HubJWTService
{
	protected $pubkey;
	protected $gzClient;
	protected $jwk_keys;

	public $rawToken;
	public $decoded;

	public function __construct ()
	{
		$this->gzClient = new Client([
			'base_uri' => config('hub3c.hubOIDServer')
		]);

		$this->getHubPublicKey();
	}

	public function getHubPublicKey ()
	{
		$endpoint = config('hub3c.hubJWKSUri');
		$request = $this->gzClient->get($endpoint);

		$this->jwk_keys = json_decode($request->getBody());
		$this->pubkey = JWK::parseKeySet($this->jwk_keys);
		
		return $this;
	}

	public function decode ($data)
	{
		if (!isset($data)) 
			return false;

		$alg = config('hub3c.hubTokenAlg');

		try 
		{
			$this->decoded = JWT::decode($data, $this->pubkey, [$alg]);

			if (!$this->decoded)
			{
				\Log::warning(__class__ . ': Decoding halted.');
				return false;
			}

			\Log::info(__class__ . ': Provided token is checked.');
			return $this;
		}
		catch (\Firebase\JWT\ExpiredException $e)
		{
			\Log::warning(__class__ . ': Provided token has expired.');
			return false;
		}
		catch (\UnexpectedValueException $e)
		{
			\Log::warning(__class__ . ' ERROR: Token is malformat.');
			return false;
		}
		catch (\ErrorException $e)
		{
			\Log::warning(__class__ . ': ERROR ' . $e->getMessage());
			return false;
		}
	}
}

<?php

namespace Hub3C\Infrastructure\Services;

use Carbon\Carbon;

class MsTzConvertService
{
	public function convertDateByMsTz (Carbon $date)//Carbon $date, $tzName)
	{
		$mstz = config('mstz');
		// Fallback
		$userTz = auth()->user()->timezone_setting or 'GMT Standard Time';
		$userTz = str_replace('.', '', $userTz); // Sometimes they send timezone data with dots.

		// Fallback
		$userTz = (array_key_exists($userTz, $mstz)) ?
				  $mstz[$userTz] :
				  'GMT Standard Time';

		return $date->setTimezone($userTz);
	}
}

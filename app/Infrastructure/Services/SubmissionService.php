<?php

namespace Hub3C\Infrastructure\Services;

use Hub3C\Domain\Submission\Models\Submission;
use Hub3C\Infrastructure\Services\AlpacaTransformerService;
use Illuminate\Http\UploadedFile;

class SubmissionService
{
	protected $alpaca;

	public function __construct (AlpacaTransformerService $alpaca)
	{
		$this->alpaca = $alpaca;
	}

	protected function buildSubmissionData ($id, $data)
	{
		$items = $data->all();
		foreach ($items as $i => $item) {
			if ($item instanceof UploadedFile) {
				$filename =
					uniqid().'.'.$data->{$i}->getClientOriginalExtension();
				$items[$i] = $data->{$i}->storeAs('submissions', $filename);
			}
		}
		$data = json_encode($items);
		return ['form_id' => $id, 'data' => $data];
	}

	protected function incrementSubmissionId ($submission)
	{
		$t = Submission::where('form_id', $submission->form_id);
		$submission->increment('form_item_id', $t->count());
	}

	public function storeSubmission ($id, $data)
	{
		$data = $this->buildSubmissionData($id, $data);
		$submission = Submission::create($data);
		// Need to do this so we can keep track on INDIVIDUAL
		// submissions from each different forms.
		$this->incrementSubmissionId($submission);
	}

	public function getAllSubmission ($limit=null)
	{
		$user = auth()->user();
		$submissions = (!isset($limit))
					   ? $user->form_submissions()
					     ->orderBy('created_at', 'desc')
					     ->get()
					   : $user->form_submissions()
					     ->orderBy('created_at', 'desc')
					     ->limit((int)$limit)
					     ->get();
		return $this->alpaca->transformAlpacaField($submissions);
	}

	public function getAllSubmissionByFormId ($id)
	{
		$user = auth()->user();
		$submissions = $user->form_submissions()->where('form_id', $id)->get();
		return $this->alpaca->transformAlpacaField($submissions);
	}

	public function getTotalSubmissions ()
	{
		$user = auth()->user();
		$totalSubmissions = $user->form_submissions()->get()->count();
		return $totalSubmissions;
	}
}

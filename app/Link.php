<?php

namespace Hub3C;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = [
        'id', 'form_id', 'user_id',
    ];
    public $incrementing = false;

    public function form() {
        return $this->belongsTo(Form::class);
    }
}

<?php

namespace Hub3C\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Hub3C\Form;

class ShareForm extends Mailable
{
    use Queueable, SerializesModels;

    var $_message = false;
    var $form = false;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message = false, $form = false)
    {
        $this->_message = $message;
        $this->form = $form;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->view('emails.form')
            ->with([
                '_message' => $this->_message,
                'form' => $this->form,
            ]);
    }
}

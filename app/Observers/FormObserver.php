<?php

namespace Hub3C\Observers;

use Hub3C\Form, Hub3C\Link;

class FormObserver {
    public function created (Form $form) {
        Link::create([
            'form_id' => $form->id,
            'user_id' => $form->user_id,
        ]);
    }
}
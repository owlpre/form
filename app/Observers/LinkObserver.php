<?php

namespace Hub3C\Observers;

use Hub3C\Link;

class LinkObserver {
    public function creating (Link $link) {
        $link->id = guid();
    }
}
<?php

return [

	// Hub's OID server

	'hubOIDServer' => env('HUB_OID_SERVER'),

	// Hub's hash algorithm.
	// See: http://119.9.27.16:60000/.well-known/openid-configuration

	'hubTokenAlg' => 'RS256',

	// Hub's JWK config URL

	'hubJWKSUri' => '.well-known/openid-configuration/jwks',

	// Hub's token cookie name

	'hubTokenCookieName' => 'HubAuthToken',

	// How long we're gonna keep the token?

	'hubTokenCookieLifetime' => 1200,

	// 3rd party API endpoint

	'hub3rdPartyEndpoint' => env('HUB_THIRDPARTY_EP'),

];

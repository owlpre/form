/**
 * @author zhixin wen <wenzhixin2010@gmail.com>
 * extensions: https://github.com/kayalshri/tableExport.jquery.plugin
 * Edited by Geekseat PHP Team for Hub3C.
 */

(function ($) {
    'use strict';
    var sprintf = $.fn.bootstrapTable.utils.sprintf;

    var TYPE_NAME = {
        json: 'JSON',
        xml: 'XML',
        png: 'PNG',
        csv: 'CSV',
        txt: 'TXT',
        sql: 'SQL',
        doc: 'MS-Word',
        excel: 'MS-Excel',
        powerpoint: 'MS-Powerpoint',
        pdf: 'PDF'
    };

    $.extend($.fn.bootstrapTable.defaults, {
        showExport: false,
        exportDataType: 'basic', // basic, all, selected
        // 'json', 'xml', 'png', 'csv', 'txt', 'sql', 'doc', 'excel', 'powerpoint', 'pdf'
        // exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel'],
        exportTypes: ['csv'],
        exportOptions: {},
        exportHiddenColumns: false, // GS/Hub3c: we declare which hidden data to include in the import
        exportExcludeColumns: false, // GS/Hub3c
        exportFilename: false // GS/Hub3c
    });

    $.extend($.fn.bootstrapTable.defaults.icons, {
        export: 'fa fa-download icon-share'
    });

    $.extend($.fn.bootstrapTable.locales, {
        formatExport: function () {
            return 'Export data';
        }
    });
    $.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales);

    var BootstrapTable = $.fn.bootstrapTable.Constructor,
        _initToolbar = BootstrapTable.prototype.initToolbar;

    BootstrapTable.prototype.initToolbar = function () {
        this.showToolbar = this.options.showExport;

        _initToolbar.apply(this, Array.prototype.slice.apply(arguments));

        if (this.options.showExport) {
            var that = this,
                $btnGroup = this.$toolbar.find('>.btn-group'),
                $export = $btnGroup.find('div.export');

            if (!$export.length) {
                $export = $([
                    '<div class="export btn-group">',
                        '<button class="btn' +
                            sprintf(' btn-%s', this.options.buttonsClass) +
                            sprintf(' btn-%s', this.options.iconSize) +
                            ' dropdown-toggle" ' +
                            'title="' + this.options.formatExport() + '" ' +
                            'data-toggle="dropdown" type="button">',
                            sprintf('<i class="%s %s"></i> ', this.options.iconsPrefix, this.options.icons.export),
                            '<span class="caret"></span>',
                        '</button>',
                        '<ul class="dropdown-menu" role="menu">',
                        '</ul>',
                    '</div>'].join('')).appendTo($btnGroup);

                var $menu = $export.find('.dropdown-menu'),
                    exportTypes = this.options.exportTypes;

                if (typeof this.options.exportTypes === 'string') {
                    var types = this.options.exportTypes.slice(1, -1).replace(/ /g, '').split(',');

                    exportTypes = [];
                    $.each(types, function (i, value) {
                        exportTypes.push(value.slice(1, -1));
                    });
                }
                $.each(exportTypes, function (i, type) {
                    if (TYPE_NAME.hasOwnProperty(type)) {
                        $menu.append(['<li data-type="' + type + '">',
                                '<a href="javascript:void(0)">',
                                    TYPE_NAME[type],
                                '</a>',
                            '</li>'].join(''));
                    }
                });

                $menu.find('li').click(function () {
                    var filename;
                    var fullDate = new Date().toISOString().slice(0,10);
                    var type = $(this).data('type'),
                        exportHiddenColumns = that.options.exportHiddenColumns, // GS/Hub3c
                        exportExcludeColumns = that.options.exportExcludeColumns, // GS/Hub3c
                        doExport = function () {
                            // GS/Hub3c
                            if (that.options.exportFilename == null) {
                                filename = type + '_export_' + fullDate;
                            } else {
                                filename = that.options.exportFilename + '-form-submission_' + fullDate;
                            }
                            if (exportExcludeColumns != null) {
                                $.each(exportExcludeColumns, function(index, value) {
                                    that.hideColumn(value);
                                });
                            }
                            // Hub3c: Set hide on unwanted ones
                            that.$el.tableExport($.extend({}, that.options.exportOptions, that.options.exportExcludeColumns, {
                                fileName: filename, // GS/Hub3c
                                type: type,
                                escape: false
                            }));
                        };

                    // Hub3c: If the hidden cols is defined, find them and set them to visible first befor do export.
                    if (exportHiddenColumns != null) {
                        $.each(exportHiddenColumns, function(index, value) {
                            that.showColumn(value);
                        });
                    }

                    if (that.options.exportDataType === 'all' && that.options.pagination) {
                        that.$el.one(that.options.sidePagination === 'server' ? 'post-body.bs.table' : 'page-change.bs.table', function () {
                            doExport();
                            that.togglePagination();
                        });
                        that.togglePagination();
                    } else if (that.options.exportDataType === 'selected') {
                        console.log("Selected only");
                        var data = that.getData(),
                            selectedData = that.getAllSelections();

                        console.log(selectedData);
                        that.load(selectedData);
                        doExport();
                        that.load(data);
                    } else {
                        doExport();
                    }

                    // Hub3c: restore unwanted cols
                    if (exportExcludeColumns != null) {
                        $.each(exportExcludeColumns, function(index, value) {
                            that.showColumn(value);
                        });
                    }  

                    // Hub3c: turn off hidden cols as the were before after the export took place.
                    if (exportHiddenColumns != null) {
                        $.each(exportHiddenColumns, function(index, value) {
                            that.hideColumn(value);
                        });
                    }

                });
            }
        }
    };
})(jQuery);

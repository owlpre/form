// For now. I will implement everything in class later.

$(function () {

	// We activate alpaca and our form builder here
	// --- NEXT SPRINT
    // $('.gsfb-alpaca').alpaca({
    // 	// data: "Geekseat Test, Y'all",
    // 	postRender: function (control) {
    // 		initAlpaca()
    // 	}
    // })

    initAlpaca()

})

var MY_LOG = '[HUB3C-FORMBUILDER] '
var GS_ELEMENT_CLASS = 'gs-form-control'

// Init our form data container
var schema = { 'type': 'object' }
var options = {}
var data = {}

function initAlpaca () {
	console.log(MY_LOG + 'Alpaca has arrived.')
	
	// Init control draggable
	initControlToolbox('basic-fields')
}

function ligtEverythingUp () {
	$('body').removeClass('fb-dropzone-hover')
	$('.fb-dropzone').removeClass('fb-dropzone-hover')
	$('.fb-dropzone').removeClass('fb-dropzone-dashed')
}

$('.fb-dropzone').droppable({
	tolerance: 'touch',
	drop: function (event, ui) {
		// UI related: Light everything up
		ligtEverythingUp()

		var draggable = $(ui.draggable)
		
		// Only receive the incoming items that is ours
		if (draggable.hasClass(GS_ELEMENT_CLASS)) {

			// Get control's data and field types:
			var dataType = draggable.attr('data-type')
			var fieldType = draggable.attr('data-field-type')

			// Previous field
			var previousField = null
			var previousFieldKey = null
		}

	},
	over: function (event, ui) {
	},
	out: function (event, ui) {
	}
})

function initControlToolbox (type) {
	var type = '.' + type

	$(type + ' .col').draggable({
		appendTo: 'body',
		helper: 'clone',
		revert: 'invalid',
		cursor: 'move',
		cursorAt: { top: -2, left: -2 },
		zIndex: 300,
		refreshPositions: true,
		start: function (event, ui) {
			$('body').addClass('fb-dropzone-hover')
			$('.fb-dropzone').addClass('fb-dropzone-hover')
			$('.fb-dropzone').addClass('fb-dropzone-dashed')
		},
		stop: function (event, ui) {
			$('body').removeClass('fb-dropzone-hover')
			$('.fb-dropzone').removeClass('fb-dropzone-hover')
			$('.fb-dropzone').removeClass('fb-dropzone-dashed')
		}
	})

	console.log(MY_LOG + 'Basic fields toolbox initialized.')
}

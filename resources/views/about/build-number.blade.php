@extends('layouts.empty')

@section('window-title', config('app.name')." version ".$number)

@section('content')
<a
    href='javascript:'
    target="_blank"
>{{ $number }}</a>
<br>
<br>
<b>
    <a style="text-decoration: none;font-size: 16px;" href="{{ url('/') }}">
        {{ 'home' }}
    </a>
</b>
@endsection
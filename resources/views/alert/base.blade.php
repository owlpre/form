@if (@request()->session()->get('alert'))
<div class="container-fluid container-alert">
    <div class="row">
        <div class="col-12">
            <div
                class="alert alert-{{ $type }} alert-dismissible fade show"
                role="alert"
                style="margin-top: 24px;"
            >
                <button
                    type="button"
                    class="close"
                    data-dismiss="alert"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
                @if (@$title)
                <h4 class="alert-heading">{{ $title }}</h4>
                @endif
                {!! $message !!}
            </div>
        </div>
    </div>
</div>
@endif

<h1>
    Form
    <u>{{ $form->title }}</u>
</h1>

{!! nl2br($_message) !!}

<hr>

<a href="{{ url('/form/'.$form->guid()) }}">Click here to open the form.</a>

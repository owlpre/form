@extends('layouts.base')

@push('styles')
    {{ Html::style('/alpaca/formbuilder/lib/alpaca/bootstrap/alpaca.css?v=2') }}
    {{ Html::style('/css/hubicon.css?v=19012017') }}
    {{ Html::style('/signature_pad/1.5.3/example/css/signature-pad.css') }}
    {{ Html::style('/css-toggle-switch-source/css/style.css') }}
    @include('layouts.base.style-designer')
    <style>
        input.cmn-toggle-round + label {
            width: 54px;
            height: 28px;
        }
        input.cmn-toggle-round + label:after {
            width: 26px;
        }
        input.cmn-toggle-round:checked + label:after {
            margin-left: 26px;
        }
        .alpaca-container-item {
            position: relative;
        }
    </style>
    @include ('style.form-extras', [
        'form_wrapper' => '#formbuilder',
    ])
@endpush

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-9 form-edit-editor-wrapper">
                <div class="text-center" id="confirmation-message">
                    All forms has been saved successfully.
                </div>
                <div id="formbuilder">
                    @include ('form.extras.header-footer.tpl', [
                        'id' => 'header'
                    ])
                    <div id="designerDiv">
                        <div class="dropzone-placeholder">
                            <h1>
                                This is your blank form.
                            </h1>
                            <p>
                                Add fields by dragging the fields from the right
                                sidebar to this area.
                            </p>
                        </div>
                    </div>
                    @include ('form.extras.header-footer.tpl', [
                        'id' => 'footer'
                    ])
                </div>
            </div>
            <nav class="col-12 col-sm-3 hidden-xs-down bg-faded side-fields">
                <div class="fb-control-deck">
                    <a
                        href="javascript:"
                        class="btn btn-secondary btn-sm"
                        id="previewForm"
                    >Preview Form</a>
                    <a
                        href="javascript:"
                        class="btn btn-sm btn-primary"
                        id="saveForm"
                    >Save Form</a>
                </div>
                <hr>
                @if (config('features.header-footer'))
                <div class="fb-control-deck">
                    <div class="fb-deck-title">Form Extras</div>
                    <div class="form-extras-wrapper">
                        @include ('form.edit.extras.header-footer')
                    </div>
                </div>
                @endif
                <div class="fb-control-deck">
                    <div class="fb-deck-title">Basic Fields</div>
                    <div id="basic" class="toolbox"></div>
                </div>
                <div class="fb-control-deck">
                    <div class="fb-deck-title">Advanced Fields</div>
                    <div id="advanced" class="toolbox"></div>
                </div>
            </nav>
        </div>
    </div>
    <div style="display: none;">
        {{ Form::open([
            'url' => 'form/preview',
            'id' => 'preview-form-post',
            'target' => '_blank',
        ]) }}
            {{ Form::text('id', $form->id) }}
            {{ Form::text('data') }}
        {{ Form::close() }}
    </div>
@stop

@push('scripts')
    <script>
        @include ('script.formbuilder._')
    </script>
    {{ Html::script('/signature_pad/1.5.3/signature_pad.js?v=1') }}
    {{ Html::script('/form/3.51/jquery.form.js') }}
    <script>
        $(function() {
            new FormBuilder(
                '{!! $form->data() !!}', '{{ csrf_token() }}'
            );
        });
    </script>
    @include ('script.form-extras', [
        'form_wrapper' => '#formbuilder',
    ])
@endpush

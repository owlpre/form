<div class="form-extras-item">
    <div class="header-footer header-footer-header">
        <table style="width: 100%;">
            <tr>
                <td>
                    Header
                </td>
                <td style="width: 50px;padding-top: 8px;">
                    <input
                        id="cmn-toggle-1"
                        class="cmn-toggle cmn-toggle-round"
                        type="checkbox"
                    >
                    <label for="cmn-toggle-1"></label>
                </td>
            </tr>
        </table>
        <div class="options" style="display: none;">
            <button
                type="button"
                class="btn btn-default btn-sm"
                data-toggle="modal"
                data-target="#choose-image-modal"
                data-selector=".header-footer-header"
            >Choose Image</button>
        </div>
    </div>
</div>
<div class="form-extras-item">
    <div class="header-footer header-footer-footer">
        <table style="width: 100%;">
            <tr>
                <td>
                        Footer
                </td>
                <td style="width: 50px;padding-top: 8px;">
                    <input
                        id="cmn-toggle-2"
                        class="cmn-toggle cmn-toggle-round"
                        type="checkbox"
                    >
                    <label for="cmn-toggle-2"></label>
                </td>
            </tr>
        </table>
        <div class="options" style="display: none;">
            <button
                type="button"
                class="btn btn-default btn-sm"
                data-toggle="modal"
                data-target="#choose-image-modal"
                data-selector=".header-footer-footer"
            >Choose Image</button>
        </div>
    </div>
</div>
<div
    class="modal fade"
    id="choose-image-modal"
    tabindex="-1"
    role="dialog"
    aria-labelledby="exampleModalLabel"
    aria-hidden="true"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5
                    class="modal-title"
                    id="exampleModalLabel"
                >Header &amp; Footer - Choose Image</h5>
                <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="image-list">
                </div>
                <br>
                {{ Form::open([
                    'url' => '/form/upload/image',
                    'files' => true,
                ]) }}
                    <div class="form-group">
                        <input
                            type="file"
                            class="form-control"
                            name="file"
                        >
                        <div
                            class="form-control-feedback"
                        ></div>
                    </div>
                {{ Form::close() }}
                <div class="progress" style="display: none;">
                    <div
                        class="progress-bar progress-bar-striped progress-bar-animated"
                        role="progressbar"
                        style="width: 50%"
                        aria-valuenow="50"
                        aria-valuemin="0"
                        aria-valuemax="100"
                    ></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-extras header-footer header-footer-{{ $id }}">
    <h1 class="form-extras-template-view text-center">
        {{ ucwords($id) }}
    </h1>
    <div class="text-center header-footer-image">
        <img>
    </div>
</div>

@extends('layouts.basic')

@section('content')
    <div class="site-wrapper">
      <div class="site-wrapper-inner">
        <div class="cover-container">
          <div class="inner cover">
            <h1 class="cover-heading">Thank you,</h1>
            <p class="lead">
              your submission has been stored successfully.
            </p>
            <p class="text-muted">
              Join <a href="http://hub3c.com/" target="_blank" title="Join Us!">Hub3c</a> today create your own forms and get other benefits for your business!
            </p>
          </div>
        </div>
      </div>
    </div>
    <footer class="footer">
        <div class="container">
            <p class="text-muted text-center">
                Powered by <span class="hub-hub"></span>
                <a href="http://hub3c.com/" target="_blank" title="Join Us!">
                    Hub3c
                </a> 
                &copy; 2017
            </p>
        </div>
    </footer>
@stop

@push('styles')
{{ Html::style('/alpaca/formbuilder/lib/alpaca/bootstrap/alpaca.css?v=2') }}
    @include('layouts.base.style-designer')
{{ Html::style('/css/hubicon.css?v=19012017') }}
<style>
html {
  position: relative;
  min-height: 100%;
}
body {
  margin-bottom: 60px;
  overflow: hidden;
}

body {
  text-align: center;
}
.site-wrapper {
  display: table;
  width: 100%;
  height: 50%;
  min-height: 50%;
}
.site-wrapper-inner {
  display: table-cell;
  vertical-align: top;
}
.cover-container {
  margin-right: auto;
  margin-left: auto;
}
.inner {
  padding: 2rem;
}
.cover {
  padding: 0 1.5rem;
}
.cover-heading {
    font-weight: bolder;
}
@media (min-width: 40em) {
  .site-wrapper-inner {
    vertical-align: middle;
  }
  .cover-container {
    width: 100%;
  }
}

@media (min-width: 62em) {
  .masthead,
  .mastfoot,
  .cover-container {
    width: 42rem;
  }
}

.footer {
    position: absolute;
    bottom: 0;
    width: 100%;
    height: 60px;
    text-transform: uppercase;
    font-size: 13px;
    letter-spacing: 2px;
}
.footer a, .footer a:visited {
    text-decoration: none;
}
</style>
@endpush

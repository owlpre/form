@extends('layouts.base')

@section('content')
    <main class="container-fluid container-index" style="padding-top:32px;">
        <div class="row">

            <div class="col-12">
                <div class="card">
                    <div
                        class="card-block" style="padding:0px !important;"
                    >
                        <table
                            data-toggle="table"
                            id="table"
                            data-classes="table table-hover table-condensed"
                            data-striped="true"
                            data-pagination="true"
                            data-page-size="4"
                            data-search="true"
                            data-page-list="[2, 4, All]"
                            data-pagination-loop="false"
                        >
                            <thead>
                                <tr>
                                    <th
                                        data-field="state"
                                        data-checkbox="true"
                                    ></th>
                                    <th
                                        data-field="id"
                                        data-visible="false"
                                    >Id</th>
                                    <th
                                        data-field="title"
                                        data-formatter="submitterFormatter"
                                    >Title</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
    </main>
@stop

@push('styles')
{{ Html::style('/css/hubicon.css?v=31012017') }}
{{ Html::style('/bootstrap-table/develop/dist/bootstrap-table.min.css') }}
{{ Html::style('/css/fbhub3c-submission.css') }}
@endpush

@push('scripts')
{{ Html::script('/bootstrap-table/develop/src/bootstrap-table.js') }}
<script>
var data = {!!$forms!!};
var selectedItems;

$(function(){
    var $table = $('#table');
    $table.bootstrapTable('load', data);

    // Custom search placeholder
    var $search = $table.data('bootstrap.table')
                .$toolbar.find('.search input');
    $search.attr('placeholder', 'Search form');
})

function submitterFormatter(value, row, index) {
    return "<a href='/form/" + (row.id) + "/edit'>" + value + "</a>"
}

</script>
@endpush

<div class="card-block-content">
    <a
        href="{{ url(
            '/form/'.$form->guid()
        ) }}"
        class="btn btn-sm btn-secondary"
        target="_blank"
        title="Open form"
    >
        <i class="fa fa-external-link">
        </i>
    </a>
    <span class="copy-to-clipboard">
        <span class="{{
            'btn btn-sm'
            .' btn-secondary'
            .' button-message'
        }}" style="display: none;">
            Copied!
        </span>
        <a
            href="javascript:"
            class="{{
                'btn btn-sm'.' btn-secondary'
                .' clipboard-js'
            }}"
            data-clipboard-text="{{ url(
                '/form/'.$form->guid()
            ) }}"
            title="Copy link"
        >
            <i class="fa fa-clipboard">
            </i>
        </a>
    </span>
    <a
        href="{{ url(
            '/form/'.$form->id.'/send'
        ) }}"
        class="btn btn-sm btn-secondary"
        title="Send via email"
    >
        <i class="fa fa-envelope">
        </i>
    </a>
    <a
        href="{{ url(
            '/form/'.$form->id.'/activate'
        ) }}"
        class="btn btn-sm btn-secondary"
        @if (@$form->active)
        title="Deactivate"
        @else
        title="Activate"
        @endif
    >
        @if (@$form->active)
        <i class="fa fa-close"></i>
        @else
        <i class="fa fa-check"></i>
        @endif
    </a>
    &nbsp;
    &nbsp;
    <a
        href="{{ url(
            '/form/'.$form->id.'/edit'
        ) }}">
        {{ $form->title }}
    </a>
    <div class="pull-right">
        @if ($form->draft)
        <span
            class="badge badge-warning"
        >Draft</span>
        @else
        <a href="{{ route(
            'submission.listByForm',
            $form->id
        ) }}"
        >
            <span
                class="badge badge-default"
            >Submissions: {{ $form->submissions->count() }}</span>
        </a>
        @endif
    </div>
</div>

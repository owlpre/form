@extends('layouts.base')

@push('scripts')
{{ Html::script('/multiple-emails.js/master/multiple-emails.js?v=1') }}
<script>
    $(function() {
        $('#multiple-emails').multiple_emails();
    });
</script>
@endpush

@push('styles')
{{ Html::style('/multiple-emails.js/master/multiple-emails.css') }}
<style>
    .multiple_emails-ul {
        margin: 0;
    }
    .multiple_emails-email {
        display: inline-block;
        margin: 1px;
    }
</style>
@endpush

@section('content')
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5
                class="modal-title"
            >
                <a href="{{ url('/') }}">
                    <i
                        class="fa fa-chevron-left"
                    ></i>
                </a>
                &nbsp;
                Share Form <b>{{ $form->title }}</b>
            </h5>
        </div>
        {{ Form::open([
            'url' => '/form/'.$form->id.'/send'
        ]) }}
            <div class="modal-body">
                    <div
                        class="form-group{{
                            $errors->has('recipient')?'has-error':''
                        }}">
                        <label for="multiple-emails">Recipient</label>
                        <input
                            id="multiple-emails"
                            type="text"
                            class="form-control"
                            name="recipient"
                            value="{{ old('recipient') }}"
                        >
                        @if ($errors->has('recipient'))
                            <span class="help-block">
                                <strong>{{ $errors->first(
                                    'recipient'
                                ) }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{
                        $errors->has('message')
                    }}">
                        <label for="message">Message</label>
                        <textarea
                            class="form-control"
                            name="message"
                            rows="9"
                        >{{ old('message') }}</textarea>
                        @if ($errors->has('message'))
                            <span class="help-block">
                                <strong>{{ $errors->first(
                                    'message'
                                ) }}</strong>
                            </span>
                        @endif
                    </div>
            </div>
            <div class="modal-footer">
                <button
                    type="submit"
                    class="btn btn-primary"
                >
                    <i class="fa fa-paper-plane"></i>
                    &nbsp;
                    Send
                </button>
            </div>
        {{ Form::close() }}
    </div>
</div>
@endsection

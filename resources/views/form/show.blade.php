@extends('layouts.basic')

@section('content')
    @if (@$preview)
    <div>
        <div class="ribbon">
            <a href="javascript:">
                <h3>Preview form</h3>
            </a>
        </div>
    </div>
    @endif
    <div class="container">
        <div class="row">
            @if (!@$preview and !@$form->active)
                <div class="col-12">
                    <h3 class="text-center" style="padding: 64px 0;">
                        Sorry, the form you're looking for is already removed by
                        the creator.
                    </h3>
                </div>
            @else
            <div
                class="col-12"
                id="form-wrapper"
            >
                @include ('form.extras.header-footer.tpl', ['id' => 'header'])
                <div class="text-center" style="padding-top: 24px;">
                    <h1>{{$form->title}}</h1>
                </div>
                <div id="form-preview"></div>
                <div style="height: 24px;"></div>
                @include ('form.extras.header-footer.tpl', ['id' => 'footer'])
            </div>
            @endif
        </div>
        <footer class="footer">
            <div class="container">
                <p class="text-muted text-center">
                    Powered by <span class="hub-hub"></span>
                    <a href="http://hub3c.com/" target="_blank" title="Join Us!">
                        Hub3c
                    </a> 
                    &copy; 2017
                </p>
            </div>
        </footer>
    </div>
@stop

@push('styles')
{{ Html::style('/alpaca/formbuilder/lib/alpaca/bootstrap/alpaca.css?v=2') }}
    @include('layouts.base.style-designer')
{{ Html::style('/css/hubicon.css?v=19012017') }}
{{ Html::style('/signature_pad/1.5.3/example/css/signature-pad.css') }}
<style>
.btn-primary:focus{
    color: #fff;
    background-color: #0275d8;
    background-image: none;
    border-color: #0275d8;
    box-shadow: none;
}
.btn-default.disabled:focus{
    background-color: #E3E4E5;
    background-image: none;
    border-color: #E3E4E5;
    box-shadow: none;
}
.ribbon {
    background-color: #a00;
    overflow: hidden;
    white-space: nowrap;
    position: fixed;
    left: -60px;
    top: 30px;
    -webkit-transform: rotate(-45deg);
     -moz-transform: rotate(-45deg);
      -ms-transform: rotate(-45deg);
       -o-transform: rotate(-45deg);
          transform: rotate(-45deg);
    -webkit-box-shadow: 0 0 10px #888;
     -moz-box-shadow: 0 0 10px #888;
          box-shadow: 0 0 10px #888;
}
.ribbon a {
    border: 1px solid #faa;
    color: #fff;
    display: block;
    font: bold 81.25% 'Helvetica Neue', Helvetica, Arial, sans-serif;
    margin: 1px 0;
    padding: 10px 50px;
    text-align: center;
    text-decoration: none;
    text-shadow: 0 0 5px #444;
}
html {
  position: relative;
  min-height: 100%;
}
body {
  margin-bottom: 60px;
  /*overflow: auto;*/
}
.footer {
    /*position: absolute;*/
    bottom: 0;
    width: 100%;
    height: 60px;
    text-transform: uppercase;
    font-size: 13px;
    letter-spacing: 2px;
}
.footer a, .footer a:visited {
    text-decoration: none;
}
</style>
@include ('style.form-extras', [
    'form_wrapper' => '#form-wrapper',
])
@endpush

@push('scripts')
@include('form.script')
{{ Html::script('/signature_pad/1.5.3/signature_pad.js?v=1') }}
@include ('script.form-extras', [
    'form_wrapper' => '#form-wrapper',
])
@endpush

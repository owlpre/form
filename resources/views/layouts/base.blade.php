<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>@yield('window-title', config('app.name') )</title>        
        @include ('style.icon')
        <script>
            window.iFrameResizer = {
                targetOrigin: '*'
            }
        </script>
        <!-- This JS file is mandatory -->
        <script src="/js/iframeResizer.contentWindow.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
        {{ Html::style('/hub3c/hubicon/hubicon.css') }}
        @include('layouts.base.style')
        <!-- todo create/use css/minify for prod env -->
        <!--
        <link rel="stylesheet" type="text/css" href="/css/hub3c.css">
        <link rel="stylesheet" type="text/css" href="/css/fbhub3c.css?v=19012017">
        -->
        @stack('styles')
    </head>
    <body>

        @include('web.partials.main-menu')

        <div style="
            height: 100vh;
            padding-top: 54px;
            overflow: auto;
        ">
            {!! alert() !!}
            @yield('content')
        </div>

        @include('script.include')
        <script>
            $(function () {
                setTimeout(function () {
                    $(".container-alert .alert").alert('close');
                }, 10000);
            });
        </script>
        @stack('scripts')
    </body>
</html>

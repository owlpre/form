<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>@yield('window-title', config('app.name') )</title>        
        @include ('style.icon')
        <script>
            window.iFrameResizer = {
                targetOrigin: '*'
            }
        </script>
        <!-- This JS file is mandatory -->
        <script src="/js/iframeResizer.contentWindow.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="/css/hub3c.css">
        <link rel="stylesheet" type="text/css" href="/css/fbhub3c.css?v=19012019">
        <style>
            body {
                min-height: initial;
            }
        </style>
        @include('layouts.base.style')
        @stack('styles')
    </head>
    <body>
        @yield('content')

        @include('script.include')
        <!-- 
        Date and datetime libs and styles.
        All 3rd party libs and css files needed by alpaca.
        -->
        <script src="/alpaca/formbuilder/libs/moment-with-locales.min.js"></script>
        <script src="/alpaca/formbuilder/libs/bootstrap-datetimepicker.min.js"></script>
        <link rel="stylesheet" media="screen" href="/css/alpaca/formbuilder/libs/bootstrap-datetimepicker.min.css"/>
        @stack('scripts')
    </body>
</html>

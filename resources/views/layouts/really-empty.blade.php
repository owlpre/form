<!DOCTYPE html>
<html>
    <head>
        <title>@yield('window-title', config('app.name') )</title>        
        @include ('style.icon')
        
        @stack('styles')
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                    @yield('content')
                </div>
            </div>
        </div>
        @stack('scripts')
    </body>
</html>

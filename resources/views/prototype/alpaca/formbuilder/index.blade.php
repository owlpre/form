<!DOCTYPE html>
<html>
    <head>
        <title>Alpaca Form Builder</title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- jQuery -->
        {{ Html::script('alpaca/formbuilder/lib/jquery/dist/jquery.min.js') }}

        <!-- Bootstrap -->
        {{ Html::style(
            'alpaca/formbuilder/lib/bootstrap/dist/css/bootstrap.css'
        ) }}
        {{ Html::style(
            'alpaca/formbuilder/lib/bootstrap/dist/css/bootstrap-theme.css'
        ) }}
        {{ Html::script(
            'alpaca/formbuilder/lib/bootstrap/dist/js/bootstrap.js'
        ) }}

        <!-- Handlebars -->
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/handlebars/handlebars.min.js'
        ) }}

        <!-- Alpaca -->
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/alpaca/bootstrap/alpaca.js'
        ) }}
        {{ Html::style(
            'alpaca/formbuilder/'
            .'lib/alpaca/bootstrap/alpaca.css'
        ) }}


        <!-- Beautify (used by EditorField) -->
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/js-beautify/js/lib/beautify.js'
        ) }}
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/js-beautify/js/lib/beautify-css.js'
        ) }}
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/js-beautify/js/lib/beautify-html.js'
        ) }}

        <!-- typeahead.js -->
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/typeahead.js/dist/bloodhound.min.js'
        ) }}
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/typeahead.js/dist/typeahead.bundle.min.js'
        ) }}

        <!-- datatables (for TableField) -->
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/datatables.net/js/jquery.dataTables.js'
        ) }}
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/datatables.net-bs/js/dataTables.bootstrap.js'
        ) }}
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/datatables.net-rowreorder/js/dataTables.rowReorder.js'
        ) }}
        {{ Html::style(
            'alpaca/formbuilder/'
            .'lib/datatables.net-bs/css/dataTables.bootstrap.css'
        ) }}

        <!-- ckeditor (for the ckeditor field) -->
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/ckeditor/ckeditor.js'
        ) }}

        <!-- fileupload control (for UploadField) -->
        {{ Html::style(
            'alpaca/formbuilder/'
            .'lib/blueimp-file-upload/css/jquery.fileupload.css'
        ) }}
        {{ Html::style(
            'alpaca/formbuilder/'
            .'lib/blueimp-file-upload/css/jquery.fileupload-ui.css'
        ) }}
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/blueimp-file-upload/js/vendor/jquery.ui.widget.js'
        ) }}
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/blueimp-file-upload/js/jquery.iframe-transport.js'
        ) }}
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/blueimp-file-upload/js/jquery.fileupload.js'
        ) }}
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/blueimp-file-upload/js/jquery.fileupload-process.js'
        ) }}
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/blueimp-file-upload/js/jquery.fileupload-ui.js'
        ) }}

        <!-- handsontable (for GridField) -->
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/handsontable/dist/jquery.handsontable.full.js'
        ) }}
        {{ Html::style(
            'alpaca/formbuilder/'
            .'lib/handsontable/dist/jquery.handsontable.full.css'
        ) }}

        <!-- moment for date and datetime controls -->
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/moment/min/moment-with-locales.min.js'
        ) }}
        <!-- bootstrap datetimepicker for date and datetime controls -->
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
        ) }}
        {{ Html::style(
            'alpaca/formbuilder/'
            .'lib/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css'
        ) }}

        <!-- bootstrap-multiselect for time field -->
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/bootstrap-multiselect/js/bootstrap-multiselect.js'
        ) }}
        {{ Html::style(
            'alpaca/formbuilder/'
            .'lib/bootstrap-multiselect/css/bootstrap-multiselect.css'
        ) }}

        <!-- jQuery Price Format for currency field -->
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/jquery-price-format2/jquery.price_format.min.js'
        ) }}

        <!-- ACE editor -->
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/ace-builds/src-min-noconflict/ace.js'
        ) }}

        <!-- jQuery UI for draggable support -->
        {{ Html::script(
            'alpaca/formbuilder/'
            .'lib/jquery-ui/jquery-ui.js'
        ) }}

        @include('prototype.alpaca.formbuilder.index.style')
        @include('prototype.alpaca.formbuilder.index.script')

    </head>
    <body data-spy="scroll" data-target=".demo-nav">

        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div>
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#source" data-toggle="tab" class="tab-item tab-item-source">Source</a></li>
                            <li><a href="#designer" data-toggle="tab" class="tab-item tab-item-designer">Designer</a></li>
                            <li><a href="#view" data-toggle="tab" class="tab-item tab-item-view">View</a></li>
                            <li><a href="#code" data-toggle="tab" class="tab-item tab-item-code">Code</a></li>
                            <li><a href="#loadsave" data-toggle="tab" class="tab-item tab-item-loadsave">Load / Save</a></li>
                        </ul>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="source">
                            <div class="row">
                                <div class="col-md-6">
                                    <div>
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#schema" data-toggle="tab" class="tab-source-schema">Schema</a></li>
                                            <li><a href="#options" data-toggle="tab" class="tab-source-options">Options</a></li>
                                            <li><a href="#data" data-toggle="tab" class="tab-source-data">Data</a></li>
                                        </ul>
                                    </div>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="schema">
                                        </div>
                                        <div class="tab-pane" id="options">
                                        </div>
                                        <div class="tab-pane" id="data">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div>
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#preview" data-toggle="tab">Preview</a></li>
                                        </ul>
                                    </div>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="preview">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="previewDiv"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="designer">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="designerDiv"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div>
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#types" data-toggle="tab">Types</a></li>
                                                    <li><a href="#basic" data-toggle="tab">Basic</a></li>
                                                    <li><a href="#advanced" data-toggle="tab">Advanced</a></li>
                                                </ul>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="types"></div>
                                                <div class="tab-pane" id="basic"></div>
                                                <div class="tab-pane" id="advanced"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="view">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="viewDiv"></div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="code">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="codeDiv"></div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="loadsave">
                            <div class="row">
                                <div class="col-md-12">

                                    <button class="btn btn-default load-button">Load Form</button>
                                    <br/>
                                    <br/>
                                    <button class="btn btn-default save-button">Save Form</button>
                                    <br/>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

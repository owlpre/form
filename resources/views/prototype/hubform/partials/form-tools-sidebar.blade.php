<nav class="col-sm-3 hidden-xs-down bg-faded sidebar">

		<div class="fb-undo-redo-block">
			<button type="button" class="btn btn-secondary btn-sm undo"><i class="hub-undo" aria-hidden="true"></i> Undo</button>
			<button type="button" class="btn btn-secondary btn-sm redo">Redo <i class="hub-redo" aria-hidden="true"></i></button>					
		</div>

		@include('prototype.hubform.partials.form-tools.basic')
		@include('prototype.hubform.partials.form-tools.advanced')

	</div>
</nav>

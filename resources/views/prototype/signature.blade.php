<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Signature Pad demo</title>
        <meta
            name="viewport"
            content="
                width=device-width,
                initial-scale=1,
                minimum-scale=1,
                maximum-scale=1,
                user-scalable=no
            ">
        {{ Html::style('/signature_pad/1.5.3/example/css/signature-pad.css') }}
    </head>
    <body onselectstart="return false">
        <div style="position: relative;">
            <div id="signature-pad" class="m-signature-pad">
                <div class="m-signature-pad--body">
                    <canvas></canvas>
                </div>
                <div class="m-signature-pad--footer">
                    <div class="description">Sign above</div>
                    <button
                        type="button"
                        class="button clear"
                        data-action="clear"
                    >Clear</button>
                    <button
                        type="button"
                        class="button save"
                        data-action="save"
                    >Save</button>
                </div>
            </div>
        </div>
        {{ Html::script('/signature_pad/1.5.3/signature_pad.js') }}
        {{ Html::script('/signature_pad/1.5.3/example/js/app.js') }}
    </body>
</html>
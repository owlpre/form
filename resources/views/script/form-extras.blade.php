<script>
$(function() {
    var selector = false;
    function ImageChooser(formExtras) {
        var modal = $("#choose-image-modal");
        var file = modal.find("input[type=file]");
        var imageList = modal.find(".image-list");
        var form = modal.find("form");
        var fetchImages = function(selector, cb) {
            $.post(
                "/user/images"
                , {_token: formExtras.token}
            ).done(function(response) {
                cb(selector, response);
            });
        };
        var refreshImages = function (selector, response) {
            if (response.result) {
                imageList.html('');
                var images = response.images;
                if (images.length) {
                    for (var i = 0; i < images.length; i++) {
                        imageList.append($(images[i]));
                    }
                    imageList.find(".image-item").on("click", function (e) {
                        var url = $(this).data('relative-path');
                        var src = $(this).attr('src');
                        formExtras.headerFooter.get(selector).setImage({
                            url: url,
                            src: src
                        });
                        modal.modal('hide');
                    });
                }
            }
        };
        file.change(function(e) {
            var self = this;
            modal.find('form').hide();
            modal.find('.progress').show();
            form.ajaxSubmit({
                success: function(data) {
                    setTimeout(function () {
                        modal.find('.progress').hide();
                        modal.find('form').show();
                    }, 500);
                    fetchImages(selector, refreshImages);
                    $(self).replaceWith(
                        $(self).val('').clone(true)
                    );
                    var errors = data.errors;
                    var inputs = form.find(':input');
                    for (var i = 0; i < inputs.length; i++) {
                        var name = $(inputs[i]).attr('name');
                        var el = $(inputs[i]);
                        var formGroup = el.parents('.form-group');
                        if (formGroup) {
                            if (errors && errors[name]) {
                                formGroup.addClass('has-danger');
                                el.addClass('form-control-danger');
                                formGroup.find('.form-control-feedback')
                                    .html(errors[name][0]);
                            } else {
                                formGroup.removeClass('has-danger');
                                el.removeClass('form-control-danger');
                                formGroup.find('.form-control-feedback')
                                    .html("");
                            }
                        }
                    }
                }
            });
        });
        modal.on('show.bs.modal', function (event) {
            selector = $(event.relatedTarget).data('selector');
            fetchImages(selector, refreshImages);
        });
    }

    function HeaderFooterToggle(selector, headerFooter, formExtras) {
        var self = this;
        var view = $(".form-extras-item .header-footer"+selector);
        var input = view.find('.cmn-toggle');
        var options = view.find(".options");
        var on = function () {
            input.attr('checked', true);
            headerFooter.on();
            options.show();
        };
        var off = function () {
            input.attr('checked', false);
            headerFooter.off();
            options.hide();
        };
        input.change(function(e) {
            if ($(this).is(":checked")) {
                on();
            } else {
                off();
            }
        });
        var initialData = formExtras.get(selector);
        if (initialData.on) {
            on();
        }
        return self;
    };

    function HeaderFooter(formExtras, selector) {
        var self = this;
        var view = {
            main: formExtras.canvas
                .find(
                    ".form-extras.header-footer"
                    +selector
                    +" .header-footer-image"
                ),
            mainImg: formExtras.canvas
                .find(
                    ".form-extras.header-footer"
                    +selector
                    +" .header-footer-image img"
                ),
            template: formExtras.canvas
                .find(
                    ".form-extras.header-footer"
                    +selector
                    +" .form-extras-template-view"
                ),
            wrapper: formExtras.canvas
                .find(
                    ".form-extras.header-footer"
                    +selector
                )
        };
        this.setImage = function (data) {
            view.template.hide();
            view.main.show();
            view.mainImg.attr("src", data.src).css("display", "inline-block");
            formExtras.save(selector, {
                src: data.src,
                url: data.url
            });
        }
        this.on = function () {
            view.wrapper.show();
            if (!view.mainImg.is(":visible")) {
                view.template.show();
            }
            formExtras.save(selector, {
                on: true
            });
        };
        this.off = function () {
            view.wrapper.hide();
            formExtras.save(selector, {
                on: false
            });
        };
        var toggle = new HeaderFooterToggle(selector, self, formExtras);
        var initialData = formExtras.get(selector);
        if (initialData.src && initialData.url) {
            self.setImage(initialData);
        }
    };

    function FormExtras(initialData, vars, token, formId) {
        var self = this;
        var imageChooser = new ImageChooser(self);
        var data = {};
        try {
            data = JSON.parse(initialData);
        } catch (e) {
            console.log(e, initialData);
        }

        this.canvas = $(vars.wrapper);
        this.token = token;
        this.vars = vars;

        this.save = function (key, value) {
            if (!data[key]) data[key] = {};
            data[key] = $.extend(data[key], value);
            (function (data) {
                var data = {
                    _token: self.token,
                    id: formId,
                    extras: JSON.stringify(data)
                };
                $.post(self.vars.URIs.postFormExtras, data);
            })(data);
        }
        this.get = function (key) {
            return data[key]?data[key]:{};
        };
        this.headerFooter = {
            header: new HeaderFooter(self, self.vars.headerFooter.header),
            footer: new HeaderFooter(self, self.vars.headerFooter.footer),
            get: function (selector) {
                if (selector === self.vars.headerFooter.header) {
                    return this.header;
                }
                return this.footer;
            }
        };
        return self;
    }

    var formExtras = new FormExtras(
        '{!! $form->extras() !!}' // initial data
        , { // vars
            wrapper: "{{ $form_wrapper }}",
            headerFooter: {
                header: ".header-footer-header",
                footer: ".header-footer-footer"
            },
            URIs: {
                postFormExtras: "/form/extras"
            }
        }
        , '{{ csrf_token() }}', {{ $form->id }}
    );
});
</script>

var FormBuilder = function(_savedConfig, formToken) {
    @include ('script.formbuilder.main')
    @include ('script.formbuilder.functions')
    @include ('script.formbuilder.engine')
    @include ('script.formbuilder.vars')
    @include ('script.formbuilder.sequence')
};

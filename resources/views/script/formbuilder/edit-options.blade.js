var editOptions = function(alpacaFieldId, callback) {
    var field = Alpaca.fieldInstances[alpacaFieldId];
    var fieldOptionsSchema = field.getSchemaOfOptions();
    var fieldOptionsOptions = field.getOptionsForOptions();
    removeFunctionFields(fieldOptionsSchema, fieldOptionsOptions);
    formatOptionsForArrayControls(field, fieldOptionsSchema)
    var fieldOptionsData = field.options;
    
    if(field.type === 'datetime') {
        enforceDatetimeProperty(fieldOptionsSchema, fieldOptionsData)
    }

    if(field.type === 'date') {
        enforceDateProperty(fieldOptionsSchema, fieldOptionsData)
    }

    if(field.type === 'country') {
        formatOptionsForCountryControl(fieldOptionsSchema)
    }

    if (field.type === 'textarea') {
        delete fieldOptionsSchema.properties.cols;
        delete fieldOptionsSchema.properties.rows;
        delete fieldOptionsSchema.properties.wordlimit;
    }

    removeUnwantedFieldsForOptions(fieldOptionsSchema)

    if (fieldOptionsSchema.properties.helpers) {
        fieldOptionsSchema.properties.helpers.title = "Tooltip";
    }
    fieldOptionsSchema.properties.validate = true
    fieldOptionsSchema.properties.showMessages = false

    if (fieldOptionsSchema.properties) {
        delete fieldOptionsSchema.properties.title;
        delete fieldOptionsSchema.properties.description;
        delete fieldOptionsSchema.properties.dependencies;
        delete fieldOptionsSchema.properties.readonly;
    }
    if (fieldOptionsOptions.fields) {
        delete fieldOptionsOptions.fields.title;
        delete fieldOptionsOptions.fields.description;
        delete fieldOptionsOptions.fields.dependencies;
        delete fieldOptionsOptions.fields.readonly;
    }
    var fieldConfig = {
        schema: fieldOptionsSchema
    };
    if (fieldOptionsOptions) {
        fieldConfig.options = fieldOptionsOptions;
    }
    if (fieldOptionsData) {
        fieldConfig.data = fieldOptionsData;
    }
    fieldConfig.view = {
        "parent": MODAL_VIEW,
        "displayReadonly": false
    };
    fieldConfig.postRender = function(control) {
        var modal = $(MODAL_TEMPLATE.trim());
        modal.find(".modal-title").append(field.getTitle() + " Options");
        modal.find(".modal-body").append(control.getFieldEl());
        modal.find('.modal-footer').append(
            "<button class='btn btn-primary pull-right okay'"
            +" data-dismiss='modal' aria-hidden='true'>Okay</button>"
        );
        modal.find('.modal-footer').append(
            "<button class='btn btn-default pull-left' data-dismiss='modal'"
            +" aria-hidden='true'>Cancel</button>"
        );
        $(modal).modal({
            "keyboard": true
        });

        var helpersSection =
            $(modal).find("[data-alpaca-container-item-name='helpers']");

        $(modal).find(".okay").click(function(e) {
            var controlValue = control.getValue();

            if (field.type === "checkbox") {
                field.options.rightLabel = controlValue.rightLabel;
                delete field.options.multiple;
            }

            if (!controlValue.label || !controlValue.label.trim()) {
                var wrapper =
                    $("[data-alpaca-container-item-name='label']");
                var el = wrapper.find('.form-group');
                el.find('small').remove();
                el.append(
                    '<small class="text-danger" style="font-size:15px;">'
                    +'Label cannot be empty.</small>'
                );
                e.stopPropagation();
                return;
            }

            if (
                !isArrayBasedControl(field)
                && typeof controlValue.enum != 'undefined'
            ) {
                if (!checkIfArrayIsUnique(controlValue.enum)) {
                    var wrapper =
                        $("[data-alpaca-container-item-name='enum']");
                    var el = wrapper.find('legend');
                    el.find('small').remove();
                    if (el.find('small').length < 1) {
                        el.append(
                            '<small class="text-danger"><br>'
                            +'The values cannot be duplicated</small>'
                        );
                    }
                } else if (checkIfArrayHasEmptyElement(controlValue.enum)) {
                    var wrapper =
                        $("[data-alpaca-container-item-name='enum']");
                    var el = wrapper.find('legend');
                    el.find('small').remove();
                    if (el.find('small').length < 1) {
                        el.append(
                            '<small class="text-danger"><br>'
                            +'The value cannot be empty</small>'
                        );
                    }
                } else {
                    field.options = control.getValue();
                    var top = findTop(field);
                    regenerate(top);
                    if (callback) {
                        callback();
                    }
                    modal.modal('hide');
                }
            } else {
                field.options = control.getValue();
                var top = findTop(field);
                regenerate(top);
                if (callback) {
                    callback();
                }
                modal.modal('hide')
            }
        });
        control.getFieldEl().find("p.help-block").css({
            "display": "none"
        });

        $(modal).find(
            ".alpaca-container-item"
            +"[data-alpaca-container-item-name='validate']"
        ).remove()
        $(modal).find(
            ".alpaca-container-item"
            +"[data-alpaca-container-item-name='showMessages']"
        ).remove()

        var helpersSection =
            $(modal).find("[data-alpaca-container-item-name='helpers']");
        helpersSection
            .find('.alpaca-container-label').css({'font-size': '15px'});
    };
    var x = $("<div><div class='fieldForm'></div></div>");
    $(x).find(".fieldForm").alpaca(fieldConfig);
};

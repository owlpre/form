var editSchema = function(alpacaFieldId, callback) {
    var field = Alpaca.fieldInstances[alpacaFieldId];
    var fieldSchemaSchema = field.getSchemaOfSchema();
    var fieldSchemaOptions = field.getOptionsForSchema();
    removeFunctionFields(fieldSchemaSchema, fieldSchemaOptions);
    formatSchema(fieldSchemaSchema)
    formatSchemaForArrayControls(field, fieldSchemaSchema)        
    var fieldData = field.schema;
    delete fieldSchemaSchema.title;
    delete fieldSchemaSchema.description;
    if (fieldSchemaSchema.properties) {
        delete fieldSchemaSchema.properties.title;
        delete fieldSchemaSchema.properties.description;
        delete fieldSchemaSchema.properties.dependencies;
    }
    var fieldConfig = {
        schema: fieldSchemaSchema
    };
    if (fieldSchemaOptions) {
        fieldConfig.options = fieldSchemaOptions;
    }
    if (fieldData) {
        fieldConfig.data = fieldData;
    }
    fieldConfig.view = {
        "parent": MODAL_VIEW,
        "displayReadonly": false
    };
    fieldConfig.postRender = function(control) {
        var modal = $(MODAL_TEMPLATE.trim());
        modal.find(".modal-title").append(field.getTitle() + " Settings");
        modal.find(".modal-body").append(control.getFieldEl());
        modal.find('.modal-footer').append(
            "<button class='btn btn-primary pull-right okay'"
            +" aria-hidden='true'>Okay</button>"
        );
        modal.find('.modal-footer').append(
            "<button class='btn btn-default pull-left'"
            +" data-dismiss='modal' aria-hidden='true'>Cancel</button>"
        );
        $(modal).modal({
            "keyboard": true
        });
        $(modal).find(".okay").click(function() {
            var controlValue = control.getValue();

            if (field.type === "checkbox") {
                if (controlValue.enum.length <= 0) {
                    field.options.multiple = false;
                } else {
                    delete field.options.multiple;
                    field.schema.enum = controlValue.enum;
                }
            }

            if (
                isArrayBasedControl(field)
                && typeof controlValue.enum != 'undefined'
            ) {
                if (!checkIfArrayIsUnique(controlValue.enum)) {
                    var wrapper =
                        $("[data-alpaca-container-item-name='enum']");
                    var el = wrapper.find('legend');
                    el.find('small').remove();
                    if (el.find('small').length < 1) {
                        el.append(
                            '<small class="text-danger"><br>'
                            +'The values cannot be duplicated</small>'
                        );
                    }
                } else if (checkIfArrayHasEmptyElement(controlValue.enum)) {
                    var wrapper =
                        $("[data-alpaca-container-item-name='enum']");
                    var el = wrapper.find('legend');
                    el.find('small').remove();
                    if (el.find('small').length < 1) {
                        el.append(
                            '<small class="text-danger"><br>'
                            +'The value cannot be empty</small>'
                        );
                    }
                } else {
                    field.schema = control.getValue();
                    var top = findTop(field);
                    regenerate(top);
                    if (callback) {
                        callback();
                    }
                    modal.modal('hide');
                }
            } else {
                field.schema = control.getValue();
                var top = findTop(field);
                regenerate(top);
                if (callback) {
                    callback();
                }
                modal.modal('hide')
            }
        });
        control.getFieldEl().find("p.help-block").css({
            "display": "none"
        });
    };
    var x = $("<div><div class='fieldForm'></div></div>");
    $(x).find(".fieldForm").alpaca(fieldConfig);
};

var dropAction = function( event, ui, form ) {
    var draggable = $(ui.draggable);
    if (draggable.hasClass("form-element")) {
        var dataType = draggable.attr("data-type");
        var fieldType = draggable.attr("data-field-type");
        var title = draggable.attr("data-title");
        var previousField = null;
        var previousFieldKey = null;
        var previousItemContainer = $(event.target).prev();

        if (previousItemContainer) {
            var previousAlpacaId = 
                $(previousItemContainer).children()
                    .first().attr("data-alpaca-field-id");
            previousField =
                Alpaca.fieldInstances[previousAlpacaId];
            previousFieldKey =
                $(previousItemContainer)
                    .attr(
                        "data-alpaca-container-item-name"
                    );
        }
        var nextField = null;
        var nextFieldKey = null;
        var nextItemContainer = $(event.target).next();
        if (nextItemContainer) {
            var nextAlpacaId = 
                $(nextItemContainer).children()
                    .first().attr("data-alpaca-field-id");
            nextField =
                Alpaca.fieldInstances[nextAlpacaId];
            nextFieldKey =
                $(nextItemContainer)
                    .attr(
                        "data-alpaca-container-item-name"
                    );
        }
        var parentFieldAlpacaId =
            $(event.target).parent()
                .parent().attr("data-alpaca-field-id");
        var parentField =
            Alpaca.fieldInstances[parentFieldAlpacaId];
        insertField(
            schema, options, data, dataType, fieldType
            , title
            , parentField, previousField, previousFieldKey
            , nextField, nextFieldKey
        );
    } else if (draggable.hasClass("interaction")) {
        var draggedIndex = $(draggable).attr("icount-ref");
        var nextItemContainer = $(event.target).next();
        var nextItemContainerIndex =
            $(nextItemContainer).attr(
                "data-alpaca-container-item-index"
            );
        var nextItemAlpacaId =
            $(nextItemContainer).children().first().attr(
                "data-alpaca-field-id"
            );
        var nextField =
            Alpaca.fieldInstances[nextItemAlpacaId];
        form.moveItem(
            draggedIndex
            , nextItemContainerIndex
            , false, function() {
                var top = findTop(nextField);
                regenerate(top);
            }
        );
    }
};

var configRefreshPostRender = function(form) {
    var iCount = 0;
    form.getFieldEl()
    .find(".alpaca-container-item").each(function() {
        formEditorItem(this, iCount);
        iCount++;
    });
    form.getFieldEl().find(".alpaca-container").addClass("dashed");
    form.getFieldEl().find(".alpaca-container-item").addClass("dashed");
    form.getFieldEl().find(".alpaca-container").each(function() {
        var containerEl = this;
        $(this).prepend("<div class='dropzone'></div>");
        $(containerEl).children(".alpaca-container-item").each(function() {
            $(this).after("<div class='dropzone'></div>");
        });
    });
    form.getFieldEl().find(".dropzone").droppable({
        "tolerance": "touch",
        "drop": function(event, ui) {
            dropAction(event, ui, form);
        },
        "over": function (event, ui ) {
            $(event.target).addClass("dropzone-hover");
        },
        "out": function (event, ui) {
            $(event.target).removeClass("dropzone-hover");
        }
    });
    form.getFieldEl().find(".interaction").draggable({
        "appendTo": "body",
        "helper": function() {
            var iCount = $(this).attr("icount-ref");
            var clone =
                $(".alpaca-container-item[icount='" + iCount + "']")
                    .clone();
            return clone;
        },
        "cursorAt": {
            "top": 100
        },
        "cursor": "move",
        "zIndex": 300,
        "refreshPositions": true,
        "start": function(event, ui) {
            $(".dropzone").addClass("dropzone-highlight");
        },
        "stop": function(event, ui) {
            $(".dropzone").removeClass("dropzone-highlight");
        }
    });
    mainDesignerField = form;
};

var formEditorItem = function(el, iCount) {
    var $el = $(el);
    var alpacaFieldId = 
        $el.children().first().attr("data-alpaca-field-id");
    $el.attr("icount", iCount);
    var width = $el.outerWidth() - 22;
    var height = $el.outerHeight() + 25;
    var cover = $("<div></div>");
    $(cover).addClass("cover");
    $(cover).attr("alpaca-ref-id", alpacaFieldId);
    $(cover).css({
        "position": "absolute",
        "margin-top": "-" + height + "px",
        "margin-left": "-10px",
        "width": '100%',
        "height": height + 10,
        "opacity": 0,
        "background-color": "white",
        "z-index": 900
    });
    $(cover).attr("icount-ref", iCount);
    $el.append(cover);
    var interaction = $("<div class='interaction'></div>");
    var buttonGroup = $(
        "<div class='pull-right field-button-group' style='margin-top:5px'>"
        +"</div>"
    );
    var schemaButton = $(
        '<button type="button" class="btn btn-circle" '
        +'style="background-color:#1cb7f1;color:#ffffff;" title="Settings"'
        +' alpaca-ref-id="'+alpacaFieldId
        +'"><i class="hubicon hub-setting"></i></button>'
    );
    buttonGroup.append(schemaButton);
    var optionsButton = $(
        '<button type="button" class="btn btn-info btn-circle" title="Options"'
        +' alpaca-ref-id="' + alpacaFieldId
        +'"><i class="hubicon hub-edit"></i></button>'
    );
    buttonGroup.append(optionsButton);
    var removeButton = $(
        '<button type="button" class="btn btn-danger btn-circle" title="Delete"'
        +' alpaca-ref-id="'+ alpacaFieldId
        +'"><i class="hubicon hub-delete"></i></button>'
    );
    buttonGroup.append(removeButton);
    interaction.append(buttonGroup);
    buttonGroup.hide()
    interaction.append("<div style='clear:both'></div>");
    $(interaction).addClass("interaction");
    $(interaction).attr("alpaca-ref-id", alpacaFieldId);
    $(interaction).css({
        "position": "absolute",
        "margin-top": "-" + height + "px",
        "margin-left": "-10px",
        "width": '100%',
        "height": height + 10,
        "opacity": 1,
        "z-index": 901
    });
    $(interaction).attr("icount-ref", iCount);
    $el.append(interaction);
    $(buttonGroup).css({
        "margin-top": $(interaction).offset(),
        "margin-right": "20px"
    });
    $(schemaButton).off().click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        var alpacaId = $(this).attr("alpaca-ref-id");
        editSchema(alpacaId);
    });
    $(optionsButton).off().click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        var alpacaId = $(this).attr("alpaca-ref-id");
        editOptions(alpacaId);
    });
    $(removeButton).off().click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        var alpacaId = $(this).attr("alpaca-ref-id");
        removeField(alpacaId);
    });
    $(interaction).hover(function(e) {
        buttonGroup.show()
        var iCount = $(interaction).attr("icount-ref");
        $(".cover[icount-ref='" + iCount + "']")
            .addClass("ui-hover-state");
    }, function(e) {
        buttonGroup.hide()
        var iCount = $(interaction).attr("icount-ref");
        $(".cover[icount-ref='" + iCount + "']")
            .removeClass("ui-hover-state");
    });
};

var refresh = function() {
    $(".dropzone").remove();
    $(".interaction").remove();
    $(".cover").remove();
    if (mainDesignerField) {
        mainDesignerField.destroy();
        mainDesignerField = null;
    }

    config.schema = schema;
    if (options) {
        config.options = options;
    }
    if (data) {
        config.data = data;
    }
    if (!config.options) {
        config.options = {};
    }
    config.options.focus = false;
    config.postRender = configRefreshPostRender;
    config.error = function(err) {
        Alpaca.defaultErrorCallback(err);
    };
    Alpaca.defaultErrorCallback = Alpaca.DEFAULT_ERROR_CALLBACK;
    $("#designerDiv").alpaca(config);
    saveData(config);
};

var mainPostRender = function(control) {
    for (var type in Alpaca.fieldClassRegistry) {
        var instance = new Alpaca.fieldClassRegistry[type]();
        var schemaSchema = instance.getSchemaOfSchema();
        var schemaOptions = instance.getOptionsForSchema();
        var optionsSchema = instance.getSchemaOfOptions();
        var optionsOptions = instance.getOptionsForOptions();
        var title = instance.getTitle();
        var description = instance.getDescription();
        var type = instance.getType();
        var fieldType = instance.getFieldType();

        var div = $(
            "<div class='form-element draggable ui-widget-content'"
            +" data-type='" + type
            + "' data-field-type='" + fieldType + "'"
            + " data-title='" + title + "'"
            + "></div>"
        );
        $(div).append(
            "<div><span class='form-element-title element-data-type-"
            + fieldType + "'></span></div>"
        );
        var isCore = isCoreField(fieldType);
        if (isCore) {
            $("#basic").append(div);
        }
        else if (isAdvancedField(fieldType)) {
            $("#advanced").append(div);
        }
        $(".form-element").draggable({
            "appendTo": "body",
            "helper": "clone",
            "zIndex": 300,
            "cursor": "move",
            "cursorAt": { top: -2, left: -2 },
            "refreshPositions": true,
            "start": function(event, ui) {
                $('.dropzone-placeholder').fadeOut('fast');
                $(".dropzone").addClass("dropzone-highlight");
            },
            "stop": function(event, ui) {
                $(".dropzone").removeClass("dropzone-highlight");

                if (
                    typeof $('.alpaca-container')
                        .attr('data-alpaca-container-item-count')
                    === 'undefined'
                ) {
                    $('.dropzone-placeholder').fadeIn('fast')
                }
            }
        });
    }
    if (
        _savedConfig && _savedConfig.options && _savedConfig.options.fields
    ) {
        for(field in _savedConfig.options.fields) {
            if (_savedConfig.options.fields[field].type == 'submittedby') {
                self.hideField('submittedby');
            }
        }
    }
};

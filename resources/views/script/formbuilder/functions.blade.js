var saveData = function (_data) {
    if (initialLoad) {
        initialLoad = false;
        return;
    }
    var i = 1;
    for(item in _data.options.fields) {
        _data.options.fields[item].order = i;
        i += 1;
    }
    $.post(
        '/form/autosave/'
        +$("#preview-form-post").find('input[name=id]').val(), {
        _token: formToken
        , data: JSON.stringify(_data)
    }).done(function (_data) {
        $('#confirmation-message').fadeIn();
        setTimeout(function () {
            $('#confirmation-message').fadeOut()
        }, 3000);
    });
};

var removeFunctionFields = function(schema, options) {
    if (schema) {
        if (schema.properties) {
            var badKeys = [];
            for (var k in schema.properties) {
                if (schema.properties[k].type === "function") {
                    badKeys.push(k);
                } else {
                    removeFunctionFields(
                        schema.properties[k]
                        , (
                            options && options.fields
                            ? options.fields[k] : null
                        )
                    );
                }
            }
            for (var i = 0; i < badKeys.length; i++) {
                delete schema.properties[badKeys[i]];
                if (options && options.fields) {
                    delete options.fields[badKeys[i]];
                }
            }
        }
    }
};

var isArrayBasedControl = function (field) {
    return field.type.match(fields.arrayBasedControls);
}

var isCoreField = function(type) {
    return type.match(fields.basic);
};

var isAdvancedField = function(type) {
    return type.match(fields.advanced);
};

this.hideField = function(type) {
    $("[data-field-type='"+type+"']").hide();
};
this.showField = function(type) {
    $("[data-field-type='"+type+"']").show();
};
this.getConfig = function() {
    return config;
}

var formatSchema = function (schema) {
    delete schema.title;
    delete schema.description;
    if (schema.properties) {
        if (schema.properties.enum) {
            schema.properties.enum.title = "Values";
        }

        delete schema.properties.title;
        delete schema.properties.description;
        delete schema.properties.dependencies;
        delete schema.properties.readonly;
        delete schema.properties.disallow;
        delete schema.properties.format;
        delete schema.properties.pattern;
        delete schema.properties.default;
    }
}

var formatSchemaForArrayControls = function (field, schema) {
    if (isArrayBasedControl(field)) {
        if (schema.properties) {
            delete schema.properties.format;
        }
    } else {
        if (schema.properties) {
            delete schema.properties.enum;
        }
    }
}

var formatOptionsForArrayControls = function (field, options) {
    delete options.properties.name;
    delete options.properties.optionLabels;
    if (isArrayBasedControl(field)) {
        if (options.properties) {
            if (options.properties.rightLabel) {
                options.properties.rightLabel.title =
                    "Option Label (If you don't use multiple value options)";
            }
            delete options.properties.dataSource;
            delete options.properties.useDataSourceAsEnum;
            delete options.properties.multiple;
            delete options.properties.removeDefaultNone;
            delete options.properties.multiselect;
            delete options.properties.emptySelectFirst;
            delete options.properties.hideNone;
            delete options.properties.noneLabel;
            delete options.properties.vertical;
        }
    }
}

var formatOptionsForCountryControl = function (options) {
    if (options.properties) {
        delete options.properties.dataSource;
        delete options.properties.useDataSourceAsEnum;
        delete options.properties.multiple;
        delete options.properties.removeDefaultNone;
        delete options.properties.multiselect;
        delete options.properties.emptySelectFirst;

        delete options.properties.hideNone;
        delete options.properties.noneLabel;
        delete options.properties.vertical;            
    }
}

var removeUnwantedFieldsForOptions = function (schemaObject) {
    if (schemaObject.properties) {
        delete schemaObject.properties.title;
        delete schemaObject.properties.description;
        delete schemaObject.properties.dependencies;
        delete schemaObject.properties.readonly;
    }
    if (schemaObject.fields) {
        delete schemaObject.fields.title;
        delete schemaObject.fields.description;
        delete schemaObject.fields.dependencies;
        delete schemaObject.fields.readonly;
    }

    delete schemaObject.properties.size
    delete schemaObject.properties.data
    delete schemaObject.properties.autocomplete
    delete schemaObject.properties.disallowOnlyEmptySpaces
    delete schemaObject.properties.disallowEmptySpaces
    delete schemaObject.properties.allowOptionalEmpty
    delete schemaObject.properties.fieldClass
    delete schemaObject.properties.maskString
    delete schemaObject.properties.inputType
    delete schemaObject.properties.view
    delete schemaObject.properties.helper
    delete schemaObject.properties.typeahead
    delete schemaObject.properties.focus
    delete schemaObject.properties.hidden
    delete schemaObject.properties.disabled
    delete schemaObject.properties.hideInitValidationError

    delete schemaObject.data
    delete schemaObject.title
    delete schemaObject.description
}

var enforceDatetimeProperty = function(datetimeSchema, datetimeData) {
    delete datetimeSchema.properties.picker
    delete datetimeSchema.properties.dateFormat
    datetimeData.dateFormat = "MM/DD/YYYY HH:mm A"
    datetimeData.picker.format = "MM/DD/YYYY HH:mm A"
}

var enforceDateProperty = function(datetimeSchema, datetimeData) {
    delete datetimeSchema.properties.picker
    delete datetimeSchema.properties.dateFormat
    datetimeData.dateFormat = "MM/DD/YYYY"
    datetimeData.picker.format = "MM/DD/YYYY"
}

@include('script.formbuilder.edit-schema')
@include('script.formbuilder.edit-options')

var insertField = function(
    schema, options, data, dataType, fieldType
    , title
    , parentField, previousField
    , previousFieldKey, nextField, nextFieldKey
){
    if (fieldType == 'submittedby') {
        self.hideField('submittedby');
    }
    var itemSchema = {
        "type": dataType
    };
    var itemOptions = {};
    if (fieldType) {
        itemOptions.type = fieldType;
    }
    itemOptions.label = "New ";
    if (title) {
        itemOptions.label += title;
    }
    else if (dataType) {
        itemOptions.label += dataType;
    }
    var itemData = null;
    var itemKey = null;
    if (parentField.getType() === "array") {
        itemKey = 0;
        if (previousFieldKey) {
            itemKey = previousFieldKey + 1;
        }
    }
    else if (parentField.getType() === "object") {
        itemKey = "new" + new Date().getTime();
    }
    var insertAfterId = null;
    if (previousField) {
        insertAfterId = previousField.id;
    }
    parentField.addItem(
        itemKey, itemSchema, itemOptions, itemData, insertAfterId
        , function() {
            var top = findTop(parentField);
            regenerate(top);
        }
    );
};

var assembleSchema = function(field, schema) {
    for (var k in field.schema) {
        if (
            field.schema.hasOwnProperty(k)
            && typeof(field.schema[k]) !== "function"
        ) {
            schema[k] = field.schema[k];
        }
    }
    schema.type = field.getType();
    delete schema.properties;
    schema.properties = {};
    if (field.children) {
        for (var i = 0; i < field.children.length; i++) {
            var childField = field.children[i];
            var propertyId = childField.propertyId;
            schema.properties[propertyId] = {};
            assembleSchema(childField, schema.properties[propertyId]);
        }
    }
};

var assembleOptions = function(field, options) {
    for (var k in field.options) {
        if (
            field.options.hasOwnProperty(k)
            && typeof(field.options[k]) !== "function"
        ) {
            options[k] = field.options[k];
        }
    }
    options.type = field.getFieldType();
    delete options.fields;
    options.fields = {};
    if (field.children) {
        for (var i = 0; i < field.children.length; i++) {
            var childField = field.children[i];
            var propertyId = childField.propertyId;
            options.fields[propertyId] = {};
            assembleOptions(childField, options.fields[propertyId]);
        }
    }
};

var findTop = function(field) {
    var top = field;
    while (top.parent) {
        top = top.parent;
    }
    return top;
};

var regenerate = function(top) {
    var _schema = {};
    assembleSchema(top, _schema);
    var _options = {};
    assembleOptions(top, _options);
    var _data = top.getValue();
    if (!_data) {
        _data = {};
    }
    schema = _schema;
    options = _options;
    data = _data;
    refresh();
};

var removeField = function(alpacaId) {
    var field = Alpaca.fieldInstances[alpacaId];
    if (field.type == 'submittedby') {
        self.showField('submittedby');
    }
    var parentField = field.parent;
    parentField.removeItem(field.propertyId, function() {
        var top = findTop(field);
        regenerate(top);
    });

    if ($('.alpaca-container').data('alpaca-container-item-count') == 1) {
        $('.dropzone-placeholder').fadeIn('fast')
    }
};

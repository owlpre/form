var self = this;

try {
    _savedConfig = JSON.parse(_savedConfig);
} catch (e) {
    console.log(e);
    return;
}

if(Object.keys(_savedConfig.schema.properties).length) {
    $(".dropzone-placeholder").hide();
}
            
var initialLoad = true;

var config = {};
var schema = _savedConfig.schema;
var options = _savedConfig.options;
var data = _savedConfig.data;

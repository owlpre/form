$('#confirmation-message').click(function(){
    $(this).hide();
})
$("#previewForm").click(function() {
    var formEl = $("#preview-form-post");
    var dataEl = formEl.find('input[name=data]');
    dataEl.val(JSON.stringify(config));
    formEl.submit();
});
$("#saveForm").click(function() {
    $.post('/form/gohome', {
        _token: formToken
    })
        .done(function (data) {
            window.location = data;
        });
});

$("<div></div>").alpaca({
    "postRender": mainPostRender
});
refresh();

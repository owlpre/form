var mainViewField = null;
var mainPreviewField = null;
var mainDesignerField = null;

var MODAL_VIEW = "bootstrap-edit";
var MODAL_TEMPLATE = ' \
    <div class="modal fade"> \
        <div class="modal-dialog modal-md"> \
            <div class="modal-content"> \
                <div class="modal-header"> \
                    <button \
                        type="button" class="close" data-dismiss="modal"\
                    >\
                        <span aria-hidden="true">&times;</span>\
                        <span class="sr-only">Close</span>\
                    </button> \
                    <h4 class="modal-title"></h4> \
                </div> \
                <div class="modal-body"> \
                </div> \
                <div class="modal-footer"> \
                </div> \
            </div> \
        </div> \
    </div> \
';

var fields = {
    basic: 
        "text|textarea|checkbox|number|radio|select|date|datetime",
    advanced:
        "personalname|submittedby|country|phone|email|url|signature|file",
    arrayBasedControls:
        "checkbox|radio|select"
};

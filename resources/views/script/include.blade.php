<?php
    $items = [
        '/js/app.js?v=3',
        '/js/jquery.min.js',
        '/js/tether.min.js',
        '/js/bootstrap.min.js',
        '/js/jquery-ui.min.js',
        '/alpaca/formbuilder/lib/handlebars/handlebars.min.js',
        '/alpacajs-build/alpaca/bootstrap/alpaca.js?v=8',
    ];
?> 

@foreach ($items as $item)
    {{ Html::script($item) }}
@endforeach

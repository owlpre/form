<script>
function checkIfArrayHasEmptyElement(items) {
    for (var i = items.length - 1; i >= 0; i--) {
        if(items[i] === "") {
            return true;
        }
    }
    return false;
}

// Check if an array is don't have a duplicated elements
function checkIfArrayIsUnique(items) {
    items.sort();    
    for ( var i = 1; i < items.length; i++ ){
        if(items[i-1] == items[i])
            return false;
    }
    return true;
}
</script>
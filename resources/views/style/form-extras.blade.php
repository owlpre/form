<style>
{{ $form_wrapper }} .form-extras .form-extras-template-view {
    border: 1px solid #e3e3e3;
    padding: 16px;
}
{{ $form_wrapper }} .form-extras.header-footer .form-extras-template-view {
    background: rgba(100, 150, 100, .1);
}

#choose-image-modal .image-list img{
    max-width: 96px;
    max-height: 96px;
}
.form-extras.header-footer {
    display: none;
}
.form-extras.header-footer .header-footer-image img {
    max-width: 100%;
    max-height: 100px;
}
.form-extras.header-footer .header-footer-image {
    display: none;
}
.form-extras.header-footer .header-footer-image .header-footer-header{
    border-bottom: 1px solid #ddd;
}
.form-extras.header-footer .header-footer-image .header-footer-footer{
    border-top: 1px solid #ddd;
}
</style>
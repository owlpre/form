<img
    src="{{ url($image) }}"
    class="image-item"
    data-relative-path="{{ $image }}"
>
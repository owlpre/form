<input type="text" id="text" focus>
<button id="change">change</button>
<div id="changed"></div>

<script type="text/javascript" src="/js/jquery.min.js"></script>
<script>
$("#change").click(function() {
    var s = $("#text").val();
    s = s.replace(/[^a-z0-9]/gi, '-').toLowerCase();
    s = s.replace(/\-+/g, '-');
    $("#changed").text(s);
});
</script>
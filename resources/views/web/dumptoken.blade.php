@extends('layouts.base')

@section('content')

    <main class="container">
    	<div class="row">

			<main class="col-sm-3 col-md-10">
				<strong>
					@if (@$currentUser)
					<div class="form-group">
						<label class="control-label">User's system Id</label>
						<br/>
						<input type="text" value="{{$currentUser->id}}">
					</div>
					<div class="form-group">
						<label class="control-label">User's firstname</label>
						<br/>
						<input
							type="text"
							value="{{$currentUser->profile->firstName}}">
					</div>
					<div class="form-group">
						<label class="control-label">User's lastname</label>
						<br/>
						<input
							type="text"
							value="{{$currentUser->profile->lastName}}">
					</div>
					<div class="form-group">
						<label class="control-label">User's email</label><br/>
						<input
							type="text"
							value="{{$currentUser->profile->emailAddress}}">
					</div>
					<div class="form-group">
						<label class="control-label">Hub Token</label><br/>
						<textarea cols="100" rows="5">
							{{Cookie::get(config('hub3c.hubTokenCookieName'))}}
						</textarea>
					</div>
					@else
						No User!
					@endif
				</strong>
			</main>
    </main>

    {{-- Sidebar is independent to each page's requirement --}}
    {{-- @include('web.partials.form-tools-sidebar') --}}

@stop

@extends('layouts.basic')

@section('content')
    <div>
        <div class="ribbon">
            <a href="javascript:">
                <h3>Preview form</h3>
            </a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div
                class="col-12"
            >
                <div id="form-preview"></div>
                <div class="text-center">
                    <div
                        class="btn btn-primary btn-lg disabled"
                        title="This is a preview form only"
                    >
                        Submit
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@push('styles')
{{ Html::style('/alpaca/formbuilder/lib/alpaca/bootstrap/alpaca.css?v=1') }}
    @include('layouts.base.style-designer')
{{ Html::style('/css/hubicon.css?v=19012017') }}
<style>
.btn-primary.active, .btn-primary:active, .show>.btn-primary.dropdown-toggle {
    color: #fff;
    background-color: #0275d8;
    background-image: none;
    border-color: #0275d8;
}
.ribbon {
    background-color: #a00;
    overflow: hidden;
    white-space: nowrap;
    position: fixed;
    left: -60px;
    top: 30px;
    -webkit-transform: rotate(-45deg);
     -moz-transform: rotate(-45deg);
      -ms-transform: rotate(-45deg);
       -o-transform: rotate(-45deg);
          transform: rotate(-45deg);
    -webkit-box-shadow: 0 0 10px #888;
     -moz-box-shadow: 0 0 10px #888;
          box-shadow: 0 0 10px #888;
}
.ribbon a {
    border: 1px solid #faa;
    color: #fff;
    display: block;
    font: bold 81.25% 'Helvetica Neue', Helvetica, Arial, sans-serif;
    margin: 1px 0;
    padding: 10px 50px;
    text-align: center;
    text-decoration: none;
    text-shadow: 0 0 5px #444;
}
</style>
@endpush

@push('scripts')
@include('form.script')
@endpush

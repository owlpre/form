<script>
jQuery.fn.injectValue = function (name, value) {
    return this.each(function () {
        var input = $("<input>").attr("type", "hidden").attr("name", name).val(value);
        $(this).append($(input));
    });
};
$(function() {
	$('alpaca-form-buttons-container').addClass('text-center');
    var data = JSON.parse('{!! $form->data() !!}');

    var controls = [];
    var formProps = {
    	"attributes": {
    		"method": "post",
    		"action": "/api/form/{!!$form->id!!}/post",
            "enctype": "multipart/form-data"
    	},
    	"buttons": {
            @if (@$form->field_count)
                @if (@$preview)
                    "noaction": {
                        "title": "Submit form",
                        "styles": "btn btn-primary btn-md disabled",
                        "attributes": {
                            "title": "This is a preview form only"
                        }
                    },
                @else
                    "submit": {
                        "title": "Submit form",
            			"styles": "btn btn-primary btn-md",
                        "value": "Submit",
            			"click": function (e) {
                            // When use pure form submit, alpaca's
                            // checkboxes will be ignored. The only way
                            // to send checkboxes is to intercept the default
                            // submit and inject the checkbox values manually
                            var $form = $('.alpaca-form');
                            var alpacaValues = this.getValue();
                            $.each(alpacaValues, function (k, v) {
                                $form.injectValue(k, v);
                            });
                            this.submit();
                            //this.submit();
            			}
            		},
                @endif
                "reset": {
                    "title": "Clear form",
                    "styles": "btn btn-default btn-md"
                }
            @else
                "noaction": {
                    "title": "Submit form",
                    "styles": "btn btn-primary btn-md disabled",
                    "attributes": {
                        "title": "This is a preview form only"
                    }
                },
                "noaction-reset": {
                    "title": "Clear form",
                    "styles": "btn btn-default btn-md disabled"
                }
            @endif
    	}
    }
    data.options.form = formProps;
    data.postRender = function(control) {
        for (var i = control.children.length - 1; i >= 0; i--) {
            if (control.children[i].signaturePad) {
                controls.push(control.children[i]);
            }
        }
    };
    $("#form-preview").alpaca(data);
});
</script>

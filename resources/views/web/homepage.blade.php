@extends('layouts.base')

@push('scripts')
{{ Html::script('/clipboard.js/1.6.0/dist/clipboard.js') }}
{{ Html::script('/form/3.51/jquery.form.js') }}
<script>
    $(function () {
        $("#create-form :input").keyup(function() {
            var form = $("#create-form");
            form.ajaxSubmit({
                url: '{{ url('/form/validate/create') }}'
                , success: function(data) {
                    var errors = data.errors;
                    var inputs = $("#create-form :input");
                    for (var i = 0; i < inputs.length; i++) {
                        var name = $(inputs[i]).attr('name');
                        var el = form.find('input[name="'+name+'"]');
                        var formGroup = el.closest('.form-group');
                        if (errors && errors.name) {
                            formGroup.addClass('has-danger');
                            el.addClass('form-control-danger');
                            formGroup.find('.form-control-feedback')
                                .html(errors[name]);
                        } else {
                            formGroup.removeClass('has-danger');
                            el.removeClass('form-control-danger');
                            formGroup.find('.form-control-feedback')
                                .html("");
                        }
                    }
                }
            });
        });
        var clipboard = new Clipboard('.clipboard-js');
        clipboard.on('success', function (e) {
            var el = $(e.trigger).closest('.copy-to-clipboard');
            el.find('.button-message').show();
            el.find('.clipboard-js').hide();
            setTimeout(function () {
                el.find('.button-message').hide();
                el.find('.clipboard-js').show();
            }, 1000);
        });
        var submitted = false;
        $("#create-form").submit(function () {
            var form = $(this);
            if (!form.attr('validated')) {
                form.ajaxSubmit({
                    url: '{{ url('/form/validate/create') }}'
                    , success: function(data) {
                        if (!data.result) {
                            var errors = data.errors;
                            for (i in errors) {
                                var el = form.find('input[name="'+i+'"]');
                                var formGroup = el.closest('.form-group');
                                formGroup.addClass('has-danger');
                                el.addClass('form-control-danger');
                                formGroup.find('.form-control-feedback')
                                    .html(errors[i]);
                            }
                        } else {
                            form.attr('validated', true);
                            form.submit();
                        }
                    }
                });
                return false;
            } else {
                if (!submitted) {
                    submitted = true;
                    return true;
                } else {
                    return false;
                }
            }
        });
        $("#load-more").click(function () {
            $.post('{{ url('/form/loadmore') }}', {
                _token: '{{ csrf_token() }}'
                , id: formView.last().id
                , fetched: formView.getForms().length
            })
            .done(function (data) {
                if (data.forms) {
                    formView.add(data.forms);
                }
                if (data.allLoaded) {
                    $("#load-more").css('visibility', 'hidden');
                }
            });
        });

        var FormView = (function () {
            var instance;
            function init() {
                function refreshView() {
                    wrapper.html('');
                    for (var i = 0; i < forms.length; i++) {
                        wrapper.append(forms[i].view);
                    }
                }
                var wrapper = $(".form-item-wrapper");
                var forms = {!! json_encode($forms) !!};
                var privateVariable = "Im also private";
                refreshView();
                return {
                    getForms: function () {
                        return forms;
                    },
                    last: function () {
                        return forms[forms.length - 1];
                    },
                    add: function (_forms) {
                        forms = forms.concat(_forms);
                        refreshView();
                    }
                };
            };
            return {
                getInstance: function () {
                    if ( !instance ) {
                        instance = init();
                    }
                    return instance;
                }
            };
        })();
        var formView = FormView.getInstance();
    });
</script>
@endpush

@push('styles')
<style>
    .card .card-block {
        padding: 0;
    }
    .card .card-block .card-block-content{
        border-bottom: 1px solid #ddd;
        padding: 1.25rem;
    }
</style>
@endpush

@section('content')
<div class="container-fluid" style="padding-top: 24px;padding-bottom: 48px;">
	<div class="row">
		<div class="col-8">
            <div class="card">
                <div class="card-block">
                    <div class="card-block-content">
                            <h4>
                            Forms
                            ({{ count(auth()->user()->forms) }})
                            <div class="pull-right">
                                <a
                                    href="javascript:"
                                    class="btn btn-success"
                                    data-toggle="modal"
                                    data-target="#create-form-modal"
                                >
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </h4>
                    </div>
                    <div class="text-center">
                        @if (!count($forms))
                        <br>
                        <br>
                        <span
                            class="hub-add-form"
                            style="font-size: 96px;"
                        ></span>
                        <br>
                        <br>
                        <p class="lead">
                            You're off to great start in creating your first
                            form. Keep it up!
                        </p>
                        <br>
                        <a
                            href="javascript:"
                            class="btn btn-success"
                            data-toggle="modal"
                            data-target="#create-form-modal"
                        >
                            Create a New Form
                        </a>
                        <br>
                        <br>
                        @else
                            <div class="text-left form-item-wrapper">
                            </div>
                            <div
                                class="text-left load-more-block"
                                style="padding: 1.25rem;"
                            >
                                <a
                                    href="javascript:"
                                    id="load-more"
                                    @if ($allLoaded)
                                    style="visibility: hidden;"
                                    @endif
                                >
                                    <span class="text-success">
                                        Load more...
                                    </span>
                                </a>
                                <a
                                    href="javascript:"
                                    class="btn btn-success pull-right"
                                    data-toggle="modal"
                                    data-target="#create-form-modal"
                                >
                                    Create a New Form
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            @include('homepage.add-form-modal')
        </div>
        <div class="col-4 col-xs-hidden">
            <div class="card">
                <div class="card-block">
                    <div class="card-block-content">
                        <h4>
                            Submissions
                            ({{app()->make('Hub3C\Infrastructure\Services\SubmissionService')->getTotalSubmissions() }})
                        </h4>
                    </div>
                    <div>
                    @foreach($submissions as $submission)
                        <div class="card-block-content">
                            <a href="/form/{{$submission['form_id']}}/submission/{{$submission['item_id']}}/view">
                                <strong>{{ $submission['submitted_by'] }}</strong>
                            </a>
                            <br>
                            <small style="text-muted">{{ $submission['date'] }}</small>
                            <br/>
                            {{ $submission['form_title'] }}
                            <div class="pull-right">
                                <i class="fa fa-chevron-right"></i>
                            </div>
                        </div>
                    @endforeach
                        <div
                            class="text-center"
                            style="padding: 1.25rem;"
                        >
                            <a
                                href="/submissions"
                            >
                                View All Submissions
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
@stop

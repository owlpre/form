<!-- Modal -->
<div
    class="modal fade"
    id="create-form-modal"
    role="dialog"
    aria-hidden="true"
>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5
                    class="modal-title"
                >
                    <span
                        class="hub-add-form"
                    ></span>
                    &nbsp;
                    Create a Form
                </h5>
                <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{ Form::open([
                'url' => '/form/store',
                'id' => 'create-form',
            ]) }}
                <div class="modal-body">
                    <div class="form-group">
                        <label
                            class="form-control-label"
                            for="name"
                        >Form Name</label>
                        <input
                            type="text"
                            class="form-control"
                            name="name"
                        >
                        <div
                            class="form-control-feedback"
                        ></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button
                        type="submit"
                        class="btn btn-primary"
                    >Build a Form</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
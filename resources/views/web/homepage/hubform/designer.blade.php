@extends('layouts.base')

@section('content')
    <div class="container-fluid confirmation" id="confirmation">
		<div class="row">
			<div class="col pull-right" style="vertical-align: right">
				<span id="confirmation-message"></span>
			</div>
			<div class="col-4 pull-left toolbox" style="vertical-align: left">
				<button type="button" class="pull-left btn btn-default btn-sm" id="previewForm" style="background: #1cb7f1">View Live Form</button>
				<button type="button" class="pull-left btn btn-default btn-sm btn-primary" id="saveForm" style="padding-right: 5px; margin-left: 15px;">Save Form</button>
			</div>
		</div>
    </div>

    <div class="container">
		<div class="row">
			<div class="col-12 col-sm-8 col-md-9" style="vertical-align: middle;padding-top: 100px;">
				<div id="designerDiv">
					<div class="dropzone-placeholder" style="position:relative;vertical-align:middle;text-align:center !important;padding-top:50px;">
						<h1 style="font-weight: bolder;">This is your blank form.</h1>
						<p>Add fields by dragging the fields from the right sidebar to this area.</p>
					</div>
				</div>
			</div>
		</div>
    </div>

    @include('prototype.hubform.partials.data-container')
    @include('prototype.hubform.partials.form-tools-sidebar')   

@stop

@push('styles')
<link rel="stylesheet" type="text/css" href="/alpaca/formbuilder/lib/alpaca/bootstrap/alpaca.css?v=2">
<link rel="stylesheet" type="text/css" href="/css/fbhub3c-designer.css?v=19012017">
<link rel="stylesheet" type="text/css" href="/css/hubicon.css?v=19012017">
@endpush

@push('scripts')
<script type="text/javascript" src="/alpaca/formbuilder/lib/handlebars/handlebars.min.js"></script>
<script type="text/javascript" src="/alpaca/formbuilder/lib/alpaca/bootstrap/alpaca.min.js"></script>
<script type="text/javascript" src="/alpaca/formbuilder/designer/hubform.js?v=19012017"></script>
@endpush

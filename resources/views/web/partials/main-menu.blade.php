<nav class="navbar navbar-toggleable-md navbar-light fixed-top bg-white nav-shadow">
    <button
        class="navbar-toggler navbar-toggler-right" 
        type="button" 
        data-toggle="collapse" 
        data-target="#navbarCollapse" 
        aria-controls="navbarCollapse" 
        aria-expanded="false" 
        aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">&nbsp;</a>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        @if(@$inside)
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a href="{{ url('/') }}">
                    <span class="nav-link">
                        <i class="fa fa-chevron-left" aria-hidden="true"></i>
                        &nbsp;
                        &nbsp;
                        {{ $form->title }}
                    </span>
                </a>
            </li>
        </ul>
        @endif
        <ul class="navbar-nav">
            <li class="nav-item">
                <a
                    class="nav-link{{ active('dashboard') }}"
                    href="{{ route('users.home') }}"
                >Dashboard</a>
            </li>
            <li class="nav-item">
                <a
                    class="nav-link{{ active('form') }}"
                    href="{{ url('/form/index') }}"
                >Forms</a>
            </li>
            <li class="nav-item">
                <a
                    class="nav-link{{ active('submission') }}"
                    href="{{ route('submission.list') }}"
                >
                    Submissions&nbsp;
                    {{-- Should've use ViewComposer here. --}}
                    <span style="color:#ffca73;font-size:13px;font-weight:bold"
                    >({{ app()->make('Hub3C\Infrastructure\Services\SubmissionService')->getTotalSubmissions() }})
                    </span>
                </a>
            </li>           
            @if(!@$inside)
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/build-number') }}">Help</a>
            </li>
            @endif
            <li class="nav-item" style="display: none;">
                <a
                    href="{{ url('/logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    Logout
                </a>
                <form
                    id="logout-form"
                    action="{{ url('/logout') }}"
                    method="POST"
                    style="display: none;"
                    >
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </div>
</nav>

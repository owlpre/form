@extends('layouts.base')

@section('content')
    <main class="container" style="padding-top:30px;">
		<div class="row">
    		<div class="col-12" style="margin-bottom: 2px">
    			@if(@$formTitle)
    				<h5>Submission details for <strong>'{{ $formTitle }}'</strong> form</h5>
    			@endif
    		</div>
			<div class="col-12 col-sm-8 col-md-9" style="vertical-align: middle;">
				<div class="card" style="width:100%;">
					<div class="card-block" id="toolbar">
						<div class="row">
							<div class="col-md-4">
								<span style="font-weight:bold;margin-top:2px;font-size:16px;">
									Submission 
									<span id="currentOffset"></span> 
									of 
									<span id="totalOffset"></span>
								</span>
							</div>
							<div class="col-md-8">
								<div class="d-flex flex-row-reverse">
									<div class="p-2 toolbar-menu">
										<div id="last">
											<a href="javascript:void(0)">
												<span class="hub-last"></span>
												<strong>Last</strong>
											</a>
										</div>
									</div>
									<div class="p-2 toolbar-menu">
										<div id="next">
											<a href="javascript:void(0)">
												<span class="hub-next"></span>
												<strong>Next</strong>
											</a>
										</div>
									</div>
									<div class="p-2 toolbar-menu">
										<div id="prev">
											<a href="javascript:void(0)">
												<span class="hub-prev"></span>
												<strong>Previous</strong>
											</a>
										</div>
									</div>									
									<div class="p-2 toolbar-menu">
										<div id="first"">
											<a href="javascript:void(0)">
												<span class="hub-first"></span>
												<strong>First</strong>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card-block" style="border-top:1px solid #cecece;padding:0px!important;">
						<table data-toggle="table"
							   style="border-radius: 0px!important;"
						       id="table-card"
						       data-classes="table table-hover table-condensed"
						       data-striped="true"
						       data-pagination="true"
						       data-only-info-pagination="true"
						       data-card-view="true"
						       data-page-size="1"
						       data-unique-id="id"
						       data-page-number="{!!$offset!!}"
							   data-pagination-first-text="First"
							   data-pagination-pre-text="Previous"
							   data-pagination-next-text="Next"
							   data-pagination-last-text="Last"
						       >
						    <thead>
						    <tr>
						        <th data-field="id" data-visible="false">ID</th>
						        @foreach($fields as $name => $mapping)
						        <th data-field="{{$name}}">{{$mapping}}</th>
						        @endforeach
						    </tr>
						    </thead>
						</table>
					</div>					
				</div>
			</div>
		</div>
    </main>

	@include('submission.partials.submission-metadata')

@stop

@push('styles')
{{ Html::style('/css/hubicon.css?v=31012017') }}
{{ Html::style('/css/bootstrap-table.min.css') }}
{{ Html::style('/css/fbhub3c-submission.css') }}
<style>
.card-view {
    clear: both;
}

.card-view .title {
    display: block;
    width: 30%;
    float: left;
}

.card-view .value {
    display: block;
    width: 500px;
    float: right;
}
</style>
@endpush

@push('scripts')
{{ Html::script('/js/bootstrap-table.min.js') }}
<script>
var data = {!!$submission!!}
var $table = $('#table-card')

$(function() {
	$table.bootstrapTable('load', data);
	var currentOffset = $(".fixed-table-body tbody tr").data("index")
	plucked = pluck(data, currentOffset)
	$('.metadata-submission-by').text(plucked.submitted_by);
	$('.metadata-submission-on').text(plucked.date);
	
	$("#currentOffset").text(currentOffset + 1)
	$("#totalOffset").text(data.length)

	// We don't have to show the Pagination
	$("#table .pull-right .pagination").remove()
})

// Imitating Laravel's pluck on JS object
var object = {
	key: function (n) {
		return this[Object.keys(this)[n]]
	}
}

function pluck (obj, idx) {
	return object.key.call(obj, idx)
}
// --

// Everytime the offset changed, we update everything
$('#table-card').on('page-change.bs.table', function (e, number, size) {
    var metadata = pluck(data, number-1)
    $('.metadata-submission-by').text(metadata.submitted_by);
    $('.metadata-submission-on').text(metadata.date);
    $("#currentOffset").text(number)
});

// Prev next etc. We have to use this to fit the layout requirement.
$('#next').click(function () {
	$table.bootstrapTable('nextPage')
})

$('#prev').click(function () {
	$table.bootstrapTable('prevPage')
})

$('#first').click(function () {
	$table.bootstrapTable('selectPage', 1)
})

$('#last').click(function () {
	$table.bootstrapTable('selectPage', data.length)
})

</script>
@endpush

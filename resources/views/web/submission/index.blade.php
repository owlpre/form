@extends('layouts.base')

@section('content')
    <main class="container-fluid container-index" style="padding-top:32px;">
    	<div class="row">
    		<div class="col-12" style="margin-bottom: 2px">
    			@if(@$formTitle)
    				<h5>Submissions for <strong>'{{ $formTitle }}'</strong> form</h5>
    			@else
    				<h5><strong>All submissions for your forms</strong></h5>
    			@endif
    		</div>
    		@if(@$byForm)
			<div id="toolbar">
			    <select class="form-control" style="font-size:13px;margin-left:4px">
			        <option value="">Export Visible</option>
			        <option value="all">Export All</option>
			        <option value="selected">Export Selected</option>
			    </select>
			</div>
			@endif
			<div class="col-12">
				<div
					class="card"
				>
					<div
						class="card-block" style="padding:0px !important;"
					><?php
					if(@$byForm) {
				        foreach($fields as $name => $mapping) {
				        	$hiddens[] = $name;
				        }
				    }
			        ?>
						<table
							data-toggle="table"
							id="table"
							@if(@$byForm)
							data-toolbar="#toolbar"
							data-toolbar-align="right"
							data-show-export="true"
							data-export-dataType="csv"
							data-export-hidden-columns={{json_encode($hiddens)}}
							data-export-exclude-columns={{json_encode($excludes)}}
							data-export-filename="{{ str_slug($formTitle) }}"
                            data-maintain-selected="true"
							@endif
							data-classes="table table-hover table-condensed"
							data-striped="true"
							data-pagination="true"
							data-page-size="10"
							data-search="true"
							data-page-list="[5, 10, 25, 50, 100, All]"
                            data-pagination-loop="false"
						>
						    <thead>
						    <tr>@if(@$byForm)
						        <th
						        	data-field="state"
						        	data-checkbox="true"
						        ></th>@endif
						        <th
						        	data-field="id"
						        	data-visible="false"
						        >Submission Id</th>
						        <th
						        	data-field="date"
						        >Date Submitted</th>@if(!@$byForm)
						        <th
						        	data-field="form_title"
						        	data-formatter="formFormatter"
						        >Form</th>@endif
						        <th
						        	data-field="submitted_by"
						        	data-formatter="submitterFormatter"
						        >Submitted by</th>
						        @if(@$byForm)
						        @foreach($fields as $name => $mapping)
						        <th data-field="{{$name}}" data-visible="false">{{$mapping}}</th>
						        @endforeach
						        @endif
						    </tr>
						    </thead>
						</table>
					</div>
				</div>
			</div>
    </main>
@stop

@push('styles')
{{ Html::style('/css/hubicon.css?v=31012017') }}
{{ Html::style('/bootstrap-table/develop/dist/bootstrap-table.min.css') }}
{{ Html::style('/css/fbhub3c-submission.css') }}
@endpush

@push('scripts')
{{ Html::script('/bootstrap-table/develop/src/bootstrap-table.js') }}
{{ Html::script('/bootstrap-table/develop/dist/extensions/export/bootstrap-table-export.js?v=3') }}
{{ Html::script('/alpaca/formbuilder/libs/tableExport.js') }}
<script>
var data = {!!$submissions!!}

$(function(){
	var $table = $('#table')
	$table.bootstrapTable('load', data);

	var $search = $table.data('bootstrap.table')
        		.$toolbar.find('.search input');
    $search.attr('placeholder', 'Search submission');

    $('#toolbar').css({'margin-right': '15px'});

    $('#toolbar').find('select').change(function () {
        $table.bootstrapTable('destroy').bootstrapTable({
        	data: data,
            exportDataType: $(this).val()
        });
    });
})

function submitterFormatter(value, row, index) {
	return "<a href='/form/" + row.form_id + "/submission/" + row.item_id + "/view'>" + value + "</a>"
}

function formFormatter(value, row, index) {
	return "<a href='/form/" + row.form_id + "/submissions'>" + value + "</a>"
}
</script>
@endpush

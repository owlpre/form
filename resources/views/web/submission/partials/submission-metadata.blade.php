<nav class="col-sm-3 hidden-xs-down bg-faded sidebar">
    <div class="fb-control-deck">
        <div class="fb-deck-title">Submission Data</div>
        <div style="font-weight:bold;color:#949da3;font-size: 14px;">Submitted by</div>
        <div class="metadata-submission-by" style="padding-bottom:30px;padding-left:20px;font-weight:bold;color:#00aeef;font-size: 14px;"></div>
        <div style="font-weight:bold;color:#949da3;font-size: 14px;">Date Submitted</div>
        <div class="metadata-submission-on" style="padding-bottom:30px;padding-left:20px;font-weight:bold;font-size: 14px;"></div>
    </div>
</nav>

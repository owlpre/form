<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// We'll check for Hub credentials on every registered user's pages.
Route::group(['middleware' => [
		// 'hub.checkTokenParam',
		'auth',
	],
			  'prefix' => '/'], function () {

	Route::get(
		'/', 
		'HomepageController@getHomepage'
	)->name('users.home');

	Route::get('/form/index', 'FormController@index')->name('form.index');
	Route::get(
		'/form/create', 
		'FormController@create'
	)->name('form.create');
	Route::post(
		'/form/store', 
		'FormController@store'
	)->name('form.store');
	Route::get(
		'/form/{form}/edit', 
		'FormController@edit'
	)->name('form.edit');
	Route::get(
		'/form/{guid}', 
		'FormController@show'
	)->name('form.show');

	Route::post('/form/extras', 'FormController@extras');

	Route::get('/images/users/{user}/{key}', 'FormController@image');
	Route::post('/user/images', 'FormController@images');

	Route::post('/form/upload/image', 'FormController@uploadImage');
	Route::post('/form/loadmore', 'FormController@loadMore');
	Route::post('/form/validate/create', 'FormController@validateFormCreate');

	Route::get('/form/{form}/activate', 'FormController@activate');

	Route::get('/form/{form}/send', 'FormController@sendto');
	Route::post('/form/{form}/send', 'FormController@send');
	Route::post('/form/preview', 'FormController@preview');
	Route::post('/form/autosave/{form}', 'FormController@autoSave');
	Route::post('/form/gohome', 'FormController@goHome');

	/* Submission */

	Route::get(
		'submissions/get/file/{key}'
		, 'SubmissionController@getFile'
	);

	Route::get(
		'submissions',
		'SubmissionController@getSubmissionList'
	)->name('submission.list');

	Route::get(
		'form/{id}/submissions',
		'SubmissionController@getSubmissionListByForm'
	)->name('submission.listByForm');

	Route::get(
		'form/{id}/submission/{offset}/view', 
		'SubmissionController@getSubmissionDetailByForm'
	)->name('submission.detailByForm');

	Route::group(['prefix' => 'submission'], function () {

		Route::get(
			'/{offset}/view', 
			'SubmissionController@getSubmissionDetail'
		)->name('submission.detail');

	});

	/* End submission */	

	// Dump Hub3C's app token -- delete/disable this route
	// on live/production.
	Route::get(
		'dumptoken',
		'DumpTokenPageController@getPage'
	)->name('break.dumptoken');

	// Protos
	Route::get(
		'/prototype/toggle'
		, 'PrototypeController@toggle'
	);
	Route::get(
	    '/prototype/signature'
	    , 'PrototypeController@signature'
	);
	Route::get(
	    '/prototype/alpaca/formbuilder'
	    , 'PrototypeController@alpacaFormBuilderIndex'
	);
	Route::get(
	    '/prototype/alpaca/formbuilder/designer'
	    , 'PrototypeController@alpacaFormBuilderDesigner'
	);
	Route::get(
		'/prototype/hubform/designer',
		'PrototypeController@getHubformPrototype'
	);

});

Route::group([
	'middleware' => 'guest',
], function () {
    // Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    // Route::post('login', 'Auth\LoginController@login');
    Route::get(
    	'/login'
    	, 'Auth\LoginController@authenticate'
    )->name('forceLogin');

});
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


Route::get('/home', 'HomeController@index');
Route::get('/build-number', 'AboutController@buildNumber');
Route::get('/tools/title-changer', 'ToolsController@titleChanger');

// Greeting page after submission
Route::get('/submission-thank-you', function() {
	return view('form.greet');
})->name('submission.status');

// This route should be used as our app main entry to accept querystring
// parameter via iframe, http://fbuilder.test/hub?token=<HUBTOKENHERE>
Route::get('/hub', 'HubAccessGatewayController@getHubTokenAndSyncUser');
